#include <list>
#include <vector>
#include <iostream>

typedef struct p_cube
{

} cube;

struct path
{
	std::string name;
	std::list<cube *> cubes; // lista di cubi appartenenti al path
	std::list<path *> paths; // lista di paths adiacenti

	struct path(std::string a): name(a){};
	struct path(){};
};

bool compare_nocase (path * first, path * second)
{
  unsigned int i=0;
  while ( (i<first->name.length()) && (i<second->name.length()) )
  {
    if (tolower(first->name[i])<tolower(second->name[i])) return true;
    else if (tolower(first->name[i])>tolower(second->name[i])) return false;
    ++i;
  }
  if (first->name.length()<second->name.length()) return true;
  else return false;
}

std::vector<cube *> * GetCubesFromPath(path p)
{
	int i=0,j=0;
	std::list<path *>::iterator it,it1,it2;
	std::cout << p.name.c_str() << std::endl;
	std::list<path *> auxList;
	for(it=p.paths.begin();it != p.paths.end();it++)
	{
		//std::cout << "Confines with: " << (*it)->name.c_str() << std::endl;
		auxList.push_back(*it);
		for(it1 = (*it)->paths.begin();it1 != (*it)->paths.end(); it1++)
		{
			if((*it1)->name.compare(p.name) != 0){
				//std::cout << (*it)->name.c_str() << "Confines with: " << (*it1)->name.c_str() << std::endl;
				auxList.push_back(*it1);
			}
		}
	}
	auxList.push_back(&p);
	auxList.sort(compare_nocase);
	auxList.unique();
	
	return NULL;
}