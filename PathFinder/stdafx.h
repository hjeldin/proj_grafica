// stdafx.h : file di inclusione per file di inclusione di sistema standard
// o file di inclusione specifici del progetto utilizzati di frequente, ma
// modificati raramente
//

#pragma once

#ifdef _MSC_VER
	#include "targetver.h"

	#define WIN32_LEAN_AND_MEAN             // Escludere gli elementi utilizzati di rado dalle intestazioni di Windows
	// File di intestazione di Windows:
	#include <windows.h>
	#include <io.h>
#endif

// File di intestazione Runtime C
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <cassert>
#include <iostream>

//Include files for GL, GLEW
#include <GL\glew.h>
#include <GL\wglew.h>
//#include <GL\glfw.h>
#include <GL\GL.h>