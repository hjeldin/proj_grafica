#include "stdafx.h"
#include "data.hpp"
#include <iostream>
#include <sstream>
#include <math.h>
#include <conio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "FrustumG.h"
#include "CTime.h"
#include "defs.h"
#include <glm/ext.hpp>
#include "AABox.h" 
int main(){
	flee::mytimer timer;
	FrustumG *fr;
	Vec3 a(0.f, 0.999999702f, 0.f);
	Vec3 b(0.800000012f, 0.f, 0.600000024f);
	Vec3 c = a*b;
	c.normalize();
	timer.Update();
	fr = new FrustumG();
	fr->setCamInternals(60.0f,800.0f/600.0f,0.01f,100.0f);
	fr->setCamDef(Vec3(0.f,0.f,-15.f),Vec3(0.f,0.f,0.f),Vec3(0.f,1.f,0.f));
	std::cout << "a box in 0,0,0; corner 10 is inside the frustum"<< (fr->boxInFrustum(AABox(Vec3(10,10,10),0,0,0)) == FrustumG::INSIDE) << std::endl;
	std::cout << "a box in -50,30,0; corner 10 is inside the frustum"<< (fr->boxInFrustum(AABox(Vec3(10,10,10),-50,30,-500)) == FrustumG::INSIDE) << std::endl;
	timer.Update();
	std::cout << timer.Get_dt() << std::endl;
	_getch();
	return 0;
}