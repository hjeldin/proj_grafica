#include "stdafx.h"
#include "Object.h"
using namespace flee;

Object::Object( float _x, float _y, float _width, float _height ) : 
	x		( _x ),
	y		( _y ),
	width	( _width ),
	height	( _height )
{
}