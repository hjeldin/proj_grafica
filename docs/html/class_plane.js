var class_plane =
[
    [ "Plane", "class_plane.html#a9289879e4f6175429b238457ebadfdfc", null ],
    [ "Plane", "class_plane.html#a2ee6656084f979accccf1ad8414a33d9", null ],
    [ "~Plane", "class_plane.html#a69abd86051c880dcb44b249ad10c4436", null ],
    [ "distance", "class_plane.html#a01aca300f4069fb0e075c6918817274f", null ],
    [ "print", "class_plane.html#aa5b806f2694d582006475cf746172641", null ],
    [ "set3Points", "class_plane.html#aefca8111e99ba60e9762cb679d8dfeff", null ],
    [ "setCoefficients", "class_plane.html#ae00628f5c4026807226e186747e8ea23", null ],
    [ "setNormalAndPoint", "class_plane.html#a301fc498bad38a9e91f0e4da936e1310", null ],
    [ "d", "class_plane.html#a61fc789fce8fbe72914f5397f1bbed44", null ],
    [ "normal", "class_plane.html#a4fca1d30bfad2542c8e38aa32ff13655", null ],
    [ "point", "class_plane.html#ab667f3427a84e74e521eaaae9bc5527f", null ]
];