var structflee_1_1float4 =
[
    [ "float4", "structflee_1_1float4.html#a3c652517ebaeb11c98666dbb2001c34d", null ],
    [ "float4", "structflee_1_1float4.html#ada496f401126b4d2293ef464c4fa6ff8", null ],
    [ "cross", "structflee_1_1float4.html#af55fd94179f91a6baaf8ed4205de823d", null ],
    [ "dot", "structflee_1_1float4.html#a37623d456812a61e1c880ecc59642d8f", null ],
    [ "magnitude", "structflee_1_1float4.html#a2b5f30cb7b81465c01a342c40f40ca9e", null ],
    [ "normalize", "structflee_1_1float4.html#ab28ddb580990669eccd45f1a95af73f6", null ],
    [ "operator*", "structflee_1_1float4.html#acabf67e4cd270b7f7728f89ec1d3123b", null ],
    [ "operator*", "structflee_1_1float4.html#ae42f266a225a20f6d1878c3f004ee3a2", null ],
    [ "operator+", "structflee_1_1float4.html#a93aebede691fc7ad4cab9f007f3e31f2", null ],
    [ "operator-", "structflee_1_1float4.html#adaf585f9aed54134e0161c9d839c4c24", null ],
    [ "operator/", "structflee_1_1float4.html#aee7a08f679eb0475d4351afe5a580983", null ],
    [ "w", "structflee_1_1float4.html#a669f09f8331b29ed1c5a78b52afd5b40", null ],
    [ "x", "structflee_1_1float4.html#a5ffbb06b9f1fbbbfdbcb98d1eb2799c2", null ],
    [ "y", "structflee_1_1float4.html#ab1801e1b1775f4f3be14da8ad0871372", null ],
    [ "z", "structflee_1_1float4.html#a5f7df74b202a7ed646f295bbbc3298e1", null ]
];