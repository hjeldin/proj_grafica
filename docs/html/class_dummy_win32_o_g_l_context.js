var class_dummy_win32_o_g_l_context =
[
    [ "DummyWin32OGLContext", "class_dummy_win32_o_g_l_context.html#a045b28f071fcfe6db5e1dcb27c7de11c", null ],
    [ "~DummyWin32OGLContext", "class_dummy_win32_o_g_l_context.html#ab94e335f36720e43b273c6d3956dfed2", null ],
    [ "Close", "class_dummy_win32_o_g_l_context.html#a822e616e29420151746986457a6c132d", null ],
    [ "Open", "class_dummy_win32_o_g_l_context.html#ae13ec6f238d0848eb577f037aa8b10be", null ],
    [ "mHGLRC", "class_dummy_win32_o_g_l_context.html#ae2d8c47d1350601234216edb831a4700", null ],
    [ "mpWglChoosePixelFormatARB", "class_dummy_win32_o_g_l_context.html#a4d8c5e16cb59daf608bd41a067e7eb31", null ],
    [ "mpWglCreateContextAttribsARB", "class_dummy_win32_o_g_l_context.html#a8dd22a794a67ab1a7e9064cf5d129341", null ]
];