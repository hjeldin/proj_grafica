#pragma once
#include "defs.h"
#include <cassert>
class DummyWin32OGLContext
{
    HWND    mHWND;

public:
	HGLRC   mHGLRC;
    PFNWGLCHOOSEPIXELFORMATARBPROC      mpWglChoosePixelFormatARB;
    PFNWGLCREATECONTEXTATTRIBSARBPROC   mpWglCreateContextAttribsARB;

public:
    DummyWin32OGLContext() :
        mHWND(NULL),
        mHGLRC(NULL),
        mpWglChoosePixelFormatARB(NULL),
        mpWglCreateContextAttribsARB(NULL)
    {
        static bool isClassReg;
        if (! isClassReg )
        {
            WNDCLASSEX wcex;
            wcex.cbSize = sizeof(WNDCLASSEX);
            wcex.style          = CS_HREDRAW | CS_VREDRAW;
            wcex.lpfnWndProc    = (WNDPROC)wndProc_s;
            wcex.cbClsExtra     = 0;
            wcex.cbWndExtra     = 0;
            wcex.hInstance      = GetModuleHandle( NULL );
            wcex.hIcon          = NULL;
            wcex.hCursor        = LoadCursor(NULL, IDC_ARROW);
            wcex.hbrBackground  = NULL;
            wcex.lpszMenuName   = NULL;
            wcex.lpszClassName  = L"DWINDOWS_TEMP_CLASS";
            wcex.hIconSm        = NULL;
            RegisterClassEx(&wcex);
        }
    }

    ~DummyWin32OGLContext()
    {
        Close();
    }

    void Open()
    {
        mHWND = CreateWindowEx(
                    0,
                    L"DWINDOWS_TEMP_CLASS",
                    L"Temp",
                    WS_OVERLAPPEDWINDOW,
                    CW_USEDEFAULT,
                    CW_USEDEFAULT,
                    CW_USEDEFAULT,
                    CW_USEDEFAULT,
                    NULL,
                    (HMENU)NULL,
                    GetModuleHandle( NULL ),
                    (LPVOID)this );

        assert( mHWND != NULL );

        mpWglChoosePixelFormatARB = (PFNWGLCHOOSEPIXELFORMATARBPROC)wglGetProcAddress( "wglChoosePixelFormatARB" );
		assert(mpWglChoosePixelFormatARB);
        mpWglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress( "wglCreateContextAttribsARB" );
		assert(mpWglCreateContextAttribsARB);
    }

    void Close()
    {
        if ( mHWND )
        {
            DestroyWindow( mHWND );
            mHWND = NULL;
        }

        if ( mHGLRC )
        {
            wglDeleteContext(mHGLRC);
            mHGLRC = NULL;
        }
    }

private:
    void onCreate( HWND hWnd )
    {
        HDC hdc = GetDC( hWnd );

        int nPixelFormat;
        static PIXELFORMATDESCRIPTOR    pfd = {
            sizeof(PIXELFORMATDESCRIPTOR),  // Size of this structure
            1,                              // Version of this structure
            PFD_DRAW_TO_WINDOW |            // Draw to Window (not to bitmap)
            PFD_SUPPORT_OPENGL |            // Support OpenGL calls in window
            PFD_DOUBLEBUFFER |              // Double buffered mode
            PFD_STEREO_DONTCARE,
            PFD_TYPE_RGBA,                  // RGBA Color mode
            32,                             // Want the display bit depth
            0,0,0,0,0,0,                    // Not used to select mode
            0,0,                            // Not used to select mode
            0,0,0,0,0,                      // Not used to select mode
            24,                             // Size of depth buffer
            8,                              // bit stencil
            0,                              // Not used to select mode
            PFD_MAIN_PLANE,                 // Draw in main plane
            0,                              // Not used to select mode
            0,0,0
        };

        nPixelFormat = ChoosePixelFormat(hdc, &pfd);

        BOOL success = SetPixelFormat(hdc, nPixelFormat, &pfd);
        if (!success)
        {
            assert( 0 );
            return;
        }

        mHGLRC = wglCreateContext( hdc );

        if (!mHGLRC ){
			std::cout << "failed"<<std::endl;
			assert(0);
		}
        wglMakeCurrent( hdc, mHGLRC );

        //wglMakeCurrent( NULL, NULL );
    }

    static LRESULT CALLBACK wndProc_s(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
    {
        if ( message == WM_CREATE )
        {
            CREATESTRUCT    *csp = (CREATESTRUCT *)lParam;
            ((DummyWin32OGLContext *)csp->lpCreateParams)->onCreate( hWnd );
        }

        return DefWindowProc(hWnd, message, wParam, lParam);
    }
};
