#pragma once

namespace flee{
class Frustum 
{
private:

	enum {
		TOP = 0,
		BOTTOM,
		LEFT,
		RIGHT,
		NEARP,
		FARP
	};


public:

	static enum {OUTSIDE, INTERSECT, INSIDE};

	Plane pl[6];


	float3 ntl,ntr,nbl,nbr,ftl,ftr,fbl,fbr;
	float nearD, farD, ratio, angle,tang;
	float nw,nh,fw,fh;

	Frustum::Frustum();
	Frustum::~Frustum();

	void Frustum::setCamInternals(float angle, float ratio, float nearD, float farD);
	void Frustum::setCamDef(float3 &p, float3 &l, float3 &u);
	int Frustum::pointInFrustum(float3 &p);
	int Frustum::sphereInFrustum(float3 &p, float raio);
	int Frustum::boxInFrustum(AABox &b);

	void Frustum::drawPoints();
	void Frustum::drawLines();
	void Frustum::drawPlanes();
	void Frustum::drawNormals();
};


};