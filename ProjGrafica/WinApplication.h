#pragma once
#include "stdafx.h" 
#include "IApplication.h"
#include "SceneManager.h"
#include "defs.h"
#include "WinInput.h"

#include "Camera.h"
#include <windows.h>
#include "HUDFBO.h"
#include "CTime.h"
#include "Object.h"
#include "ThirdPersonCamera.h"
#include "StaticShadowMap.h"
#include "DebugTextureRenderer.h"
#include "WinOGLContext.h"

namespace flee{
	class WinApplication : IApplication
	{
	public:
		WinApplication(void);
		~WinApplication(void);

		void Init(void);
		void Deinit(void);
		void Render(void);
		void RenderPass(void);
		void ShadowPass(void);
		void HUDPass(void);
		void Update(void);
		void SetHwnd(HWND);
		void SetHinstance(HINSTANCE);
		void Resize(int, int);
		bool hasFocus;
		bool running;
		int Camera_Collide(flee::Object *object, flee::float3 futurepos, flee::float3 camvel, float dt);
		int CameraCollision(flee::float3 futcampos);
		int Model_Collide(flee::Object *Wall, flee::Object *Model);
		int ModelCollision(flee::float3 futcampos);

	private:
		DebugTextureRenderer * dtr;
		bool generatedShadows;
		SceneManager * sceneMan;		// Scene manager
		Camera * camera;
		ThirdPersonCamera * thpCamera;
		void * cameraPtr;
		WinOGLContext * context;		// Context creator. Rendering occurs here.
		//Timer * timer;				// High resolution timer
		mytimer timer;
		WinInput * inputMan;			// Input manager
		HINSTANCE hInstance;			// Module instance
		HWND hWnd;						// Window handle
		HUDFBO hfbo;
		Mesh * myMesh;					//XXX: should be moved and deleted 
		bool draw;
		bool in_map;
		bool tpc;
		flee::float3 overlap;
		Object *obj;
		Object *player;
		Quadtree *qt;
		float3 *newpos;
		float3 translation;
		vector<flee::Object *> vobj;
		vector<flee::Object *>::iterator it;
		StaticShadowMap shadowMap;
	};
}