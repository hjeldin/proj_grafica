#pragma once
#include "Mesh.h"
#include "defs.h"
#include "ShaderManager.h"

namespace flee{
	class DebugTextureRenderer
	{
	public:
		DebugTextureRenderer(void);
		~DebugTextureRenderer(void);
		void Init(void);
		void Deinit(void);
		virtual void GenerateShader(std::string vSh, std::string pSh, std::string gSh);
		void RenderTexture(GLuint textureToRender);
	protected:
		GLuint textureToDraw, vShader, pShader,gShader, program, texLocation,centerPoint;
	};
};

