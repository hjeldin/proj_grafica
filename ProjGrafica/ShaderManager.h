#pragma once

#include <vector>
#include <string>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include "LightManager.h"
namespace flee{
	/**
		Singleton shader manager.
		Every shader and every ''common'' uniform should be created/modified/deleted from here
		\warning Lots of SetUniform are broken and should not be used
	*/
	class ShaderManager
	{
	public:
		/**
			Singleton access to shader manager
			\return pointer to instance
		*/
		static ShaderManager* get_instance()
		{
			if(instance_ptr == NULL){
				instance_ptr = new ShaderManager();
			}
			return instance_ptr;
		}
		
		~ShaderManager(void);													///Default deconstructor
		/**
			Compiles a shader.
			\param type the GLenum type (vertex,geometry,fragment)
			\param shader a pointer to the string containing the shader
			\return GLuint unique number of shader
		*/
		GLuint CompileShaderOfType(GLenum type, std::string & shader);	
		/**
			Links the shaders into a program
			\param mVertexShader identifier to vertex shader
			\param mFragmentShader identifier to fragment shader
			\param geometryShader indentifier to geometry shader (usually null)
			\return the compiled program identifier
		*/
		GLuint LinkShaders(GLuint mVertexshader, GLuint mFragmentShader, GLuint geometryShader);
		void SetUniforms(int program, glm::mat4 model, glm::mat4 view, glm::mat4 projection);
		void SetUniforms(int program, glm::mat4 model, glm::mat4 view);
		void SetUniforms(int program, glm::mat4 model);
		/**
			Sets for every shader into this manager the specified view
			\param view view matrix
		*/
		void SetUniforms(glm::mat4 view);
		/**
			Sets for every shader into this manager the specified projection
			\param proj projection matrix
		*/
		void SetProjection(glm::mat4 proj);
		void GetUniforms(int program);
		/**
			Adds the specified program into shaderPrograms vector
			\param program the program to be inserted
		*/
		int AddProgram(int program);

		/**
			Uses specified program.
			\param program the program to be used
		*/
		void UseProgram(int program);

		/**
			Gets low level identifier to specified program
			\param program the program to get
		*/
		GLuint GetProgram(int program);
		void SetShadowMVP(glm::mat4 mat);	
		void SetLightsUniform(const std::vector<Light *> * lights);
		void SetPlayerPos(glm::vec3 position);
	private:
		ShaderManager(void);										///default constructor
		static ShaderManager * instance_ptr;						///Instance pointer to singleton access
		std::vector<GLuint> shaderPrograms;
		std::vector<GLuint> positionAttribLocation;
		std::vector<GLuint> colorAttribLocation;
		std::vector<GLuint> modelMatrixLocation;
		std::vector<GLuint> viewMatrixLocation;
		std::vector<GLuint> projectionMatrixLocation;
	};
}
