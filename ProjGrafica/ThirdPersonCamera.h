#pragma once
#include "camera.h"
#include "Model.h"
namespace flee{
class ThirdPersonCamera :
	public Camera
{
public:
	ThirdPersonCamera(void);
	~ThirdPersonCamera(void);
	void Follow(Model *);
	void Translate(float3 t);
	void SetPosition(float3 t);
	void LookAt(int x, int y);
	float3 FakeTranslate(float3 t);
private:
	Model * following;
};
};
