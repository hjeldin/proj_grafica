#include "StdAfx.h"
#include "IOGLContext.h"
#include "DummyOGLContext.h"
#include "SceneManager.h"
namespace flee{
	/**
	OpenGLContext is a class designed to store all of your OpenGL functions and keep them
	out of the way of your application logic. Here we have the ability to create an OpenGL
	context on a given window and then render to that window.
	*/
	class WinOGLContext : IOGLContext {
	public:
		WinOGLContext(void);				/// Default constructor
		WinOGLContext(HWND hwnd);			/// Constructor for creating our context given a hwnd
		~WinOGLContext(void);				/// Destructor for cleaning up our application
		bool create30Context();				/// Creation of our OpenGL 3.x context
		bool createContext();				/// Creation of standard OpenGL context
		bool InitializeExtensions();		/// Initialize opengl extension wrangler
		void setupScene(void);				/// All scene information can be setup here
		void reshapeWindow(int w, int h);	/// Method to get our window width and height on resize
		void renderScene(void);				/// Render scene
		void CheckErrors(void);				/// Check for opengl errors
		void SetHandle(HWND handle);		/// Sets window handle
		int windowWidth;				/// Store the width of our window
		int windowHeight;			/// Store the height of our window
		void Swap(void);
		void setWindowTitle(std::string);
		SceneManager * manager;
	private:
	protected:
		HGLRC dummyHrc;						/// Dummy rendering context
		HGLRC hrc;							/// Rendering context
		HDC hdc;							/// Device context
		HWND hwnd;							/// Window handle
		char g_szAAPixelFormat[32];			/// Array for pixel format
	};
};