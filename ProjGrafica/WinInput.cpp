#include "stdafx.h"
#include "WinInput.h"

using namespace flee;

WinInput::WinInput(void)
{
	current_state.keyboard = (flee::k_state *)malloc(sizeof(k_state));
	current_state.mouse = (flee::m_state *)malloc(sizeof(m_state));

	previous_state.keyboard = (flee::k_state *)malloc(sizeof(k_state));
	previous_state.mouse = (flee::m_state *)malloc(sizeof(m_state));
}


WinInput::~WinInput(void)
{
	free(current_state.keyboard);
	free(current_state.mouse);
	free(previous_state.keyboard);
	free(previous_state.mouse);
}

// this is the function that initializes DirectInput
void WinInput::Init(HINSTANCE hInstance, HWND hWnd, int width, int height)
{
	// create the DirectInput interface
	DirectInput8Create(hInstance,			// the handle to the application
					DIRECTINPUT_VERSION,    // the compatible version
					IID_IDirectInput8,		// the DirectInput interface version
					(void**)&din,			// the pointer to the interface
					NULL);					// COM stuff, so we'll set it to NULL

	// create the keyboard device
	din->CreateDevice(GUID_SysKeyboard,		// the default keyboard ID being used
					&dinkeyboard,			// the pointer to the device interface
					NULL);					// COM stuff, so we'll set it to NULL

	din->CreateDevice(GUID_SysMouse,
                       &dinmouse,
                       NULL);

     // set the data formats
    dinkeyboard->SetDataFormat(&c_dfDIKeyboard);
    dinmouse->SetDataFormat(&c_dfDIMouse);

	// set the control you will have over the keyboard
	dinkeyboard->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	dinmouse->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	
	this->hWnd = hWnd;
	this->max_x = width;
	this->max_y = height;
}

// this is the function that gets the latest input data
input_state * WinInput::detect_input(void)
{
    // get access if we don't have it already
    dinkeyboard->Acquire();
	dinmouse->Acquire();
    // get the input data
    dinkeyboard->GetDeviceState(256, (LPVOID)keystate);
	dinmouse->GetDeviceState(sizeof(DIMOUSESTATE),(LPVOID)&mousestate);

	previous_state.keyboard->state = keystate;
	previous_state.mouse->state = &mse;

	mse.state = mousestate;
	mse.x += mse.state.lX;
    mse.y += mse.state.lY;

    // to constrain mouse cursor position to the screen bounding box
    // uncomment the following lines and define screenWidth and screenHeight
    
    if (mse.x <= 0)
        mse.x = 0;
    if (mse.x > this->max_x)
        mse.x = max_x;
    if (mse.y <= 0)
        mse.y = 0;
	if (mse.y > this->max_y)
        mse.y = max_y;

    mse.pin = 0;
	if (mse.state.rgbButtons[0] && !mse.pinstate) {
        mse.pinstate = 1;
        mse.pin = 1;
    }

    // reset 'pin' state
    if (!mse.state.rgbButtons[0])
        mse.pinstate = 0;

	current_state.keyboard->state = keystate;
	current_state.mouse->state = &mse;

	return &current_state;
}

// this is the function that closes DirectInput
 void WinInput::cleanDInput(void)
 {
     dinkeyboard->Unacquire();		// make sure the keyboard is unacquired
	 dinmouse->Unacquire();
     din->Release();				// close DirectInput before exiting
 }