#pragma once
#include <string>
#include <fstream>
#include "ShaderManager.h"
#include "Model.h"
#include "DebugTextureRenderer.h"

namespace flee{
	/**
		HUD(Minimap) Frame Buffer Object.

		Purpose:
		Workflow should be the following:
		- Generate framebuffer/textures and whatever
		- Bind texture to framebuffer
		- for each draw
			- bind framebuffer
			- set uniforms on rendering shader
			- render to texture(framebuffer)
			- debind buffer
			- bind texture
			- draw quad
			- debind texture
		Last three points are done extending DebugTextureRenderer
	*/
	class HUDFBO : public DebugTextureRenderer
	{
	public:
		/**
			Default constructor
		*/
		HUDFBO(void);
		/**
			Default destructor
		*/
		~HUDFBO(void);
		/**
			Initializes shaders, textures and framebuffer
		*/
		bool Init();
		/**
			Renders specified mesh on hud
			\param modelVec pointer to mesh to render
		*/
		void Render(Mesh * modelVec);

		/**
			Sets projection matrix 
			\param proj matrix 
		*/
		void SetProjection(glm::mat4 proj);
		/**
			Sets view matrix 
			\param view matrix 
		*/
		void SetView(glm::mat4 view);
		/**
			Sets model matrix 
			\param model matrix 
		*/
		void SetModel(glm::vec3 model);
    private:
        GLuint m_fbo;
		GLuint quadTexSample;
        GLuint m_HUD;
		GLuint shaderProgram,mFs,mVs,qmfs,qmvs,quadShaderProgram;
		GLuint positionAttrib,colorAttrib,modelMatrix,viewMatrix,projectionMatrix;
		glm::mat4x4 modelM,viewM,projM;
		std::string shaders[2];
		std::vector<Model *>::iterator itVec;
	};
};