#pragma once
#include "Material.h"
#include <string>
#include <map>

namespace flee{
	/**
		Singleton material manager.
		
		Tracks every used material and dispose them when needed (usually at destruction).
	*/
	class MaterialManager
	{
	public:
		/**
			Default destructor
		*/
		~MaterialManager(void);
		/**
			Singleton access to material manager
		*/
		static MaterialManager* get_instance()
		{
			if(instance_ptr == NULL){
				instance_ptr = new MaterialManager();
			}
			return instance_ptr;
		}
		/**
			Adds a material on the map
		*/
		void Add(Material *);
		/**
			Removes a material from the map
		*/
		void Remove(Material *);
		/**
			Delete all materials
		*/
		void Purge();
		/**
			Getter for a specified material
			\param str key to search in the map
		*/
		Material * Get(std::string &str);
	private:
		MaterialManager(void);
		std::map<std::string, Material *> materialMap; ///Material map, contains all materials
		static MaterialManager * instance_ptr; ///Instance pointer for singleton
	};
}