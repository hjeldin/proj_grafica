#pragma once
#include <vector>
#include "Model.h"
using namespace std;
	namespace flee{
	class Quadtree;
	class Object;

	class Quadtree {
	public:
		Quadtree( float x, float y, float width, float height, int level, int maxLevel );
		void AddObject( Object *object );
		vector<Object*> GetObjectsAt( float x, float y );
		void Clear();

	private:
		float x;
		float y;
		float width;
		float height;
		int level;
		int maxLevel;
		vector<Object*> objects;

		Quadtree * parent;
		Quadtree * NW;
		Quadtree * NE;
		Quadtree * SW;
		Quadtree * SE;

		bool Contains( Quadtree *child, Object *object );
	};
};