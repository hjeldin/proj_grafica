#include "stdafx.h"
#include "DebugTextureRenderer.h"
using namespace flee;
DebugTextureRenderer::DebugTextureRenderer(void)
{

}


DebugTextureRenderer::~DebugTextureRenderer(void)
{

}

void DebugTextureRenderer::Init(void)
{

}

void DebugTextureRenderer::Deinit(void)
{
	glDeleteShader(vShader);
	glDeleteShader(pShader);
	glDeleteProgram(program);
}

void DebugTextureRenderer::GenerateShader(std::string vSh, std::string pSh, std::string gSh)
{
	 vShader = ShaderManager::get_instance()->CompileShaderOfType(GL_VERTEX_SHADER, file2string(vSh));
	 pShader = ShaderManager::get_instance()->CompileShaderOfType(GL_FRAGMENT_SHADER, file2string(pSh));
	 gShader = ShaderManager::get_instance()->CompileShaderOfType(GL_GEOMETRY_SHADER, file2string(gSh));
	 program = ShaderManager::get_instance()->LinkShaders(vShader,pShader,gShader);
	 program = ShaderManager::get_instance()->GetProgram(program);
	 glUseProgram(program);
	 centerPoint = glGetUniformLocation(program,"centerPoint");
	 texLocation = glGetUniformLocation(program, "texSampler");
}

void DebugTextureRenderer::RenderTexture(GLuint textureToRender)
{
	glUseProgram(program);
	glBindTexture (GL_TEXTURE_2D, textureToRender);
	glUniform1i(texLocation,0);
	glUniform4f(centerPoint,0.4,0.4,0.4,0.4);
	glDrawArrays(GL_POINTS, 0, 1);
}