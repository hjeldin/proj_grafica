#include "StdAfx.h"
#include "SceneManager.h"
#include "Mesh.h"
#include <cmath>
#include "Animations.hpp" 
#include "SkyBox.h"
#include "AABox.h"
#include "Vec3.h"	

using namespace flee;

SceneManager::SceneManager()
{
	//this->app = app;
	//this->device = app->GetDevice();
	qt = new Quadtree(0,0,328,328,0,3);
}

void SceneManager::Init(void)
{
	skybox = new SkyBox();
	skybox->Load(std::string("textures\\skybox_xp.bmp"),std::string("textures\\skybox_xn.bmp"),std::string("textures\\skybox_yp.bmp"),std::string("textures\\skybox_yn.bmp"),std::string("textures\\skybox_zp.bmp"),std::string("textures\\skybox_zn.bmp"));
	lightMan.CreateLight(glm::vec3(81,3,3),glm::vec3(0,0,0),glm::vec3(1,1,1));
	return;
}
bool SceneManager::ImportAsset( const std::string& pFile)
{
	scene = importer.ReadFile( pFile,aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_SortByPType);
	if( !scene)
	{
		return false;
	}
	return true;
}

void SceneManager::SetWindow(int w, int h){ 
	width = w; 
	height=h; 
	glm::mat4 Projection = glm::perspective(75.0f,((float)w/(float)h), 0.1f, 100.0f);
	glUniformMatrix4fv(projectionUniformLocation , 1, GL_FALSE, glm::value_ptr(Projection));	
}

void SceneManager::ElaborateScene()
{
	unsigned int index=0;
	if(scene->HasMeshes())
	{
		//for(index=0;index<scene->mNumMeshes;index++)
		Material * mat = new Material(scene->mMaterials[0]);

		Mesh * mesh = new Mesh(scene->mMeshes[0]);
		matMan = MaterialManager::get_instance();
		meshMan = MeshManager::get_instance();
		//time.Initialize();
		std::ifstream map;
		std::string line;
		int index = 0, row = 0;
		//float s_index = 0, s_row = 0;
		float max_x=0,max_z=0;
		char t;
		vector<float> ts_index, ts_row, tf_index, tf_row;
		bool r = false;
		int k = 0;

		mesh->InitializeBuffers();
		//BufferManager::get_instance()->Remove(mesh);
		map.open("coordinates.txt",ios::in);

		if(!map.is_open())
		{
			exit(1);//gestione errore
		}
		vector<vertex> *vertici = new vector<vertex>();
		vector<int> *indici = new vector<int>();
		int vertice_base = 0;

		while(getline(map,line))
		{
			std::istringstream(line,ios_base::in) >> index >> row >> t;
			if(t == 's')
			{
				ts_index.push_back(index);
				ts_row.push_back(row);
				r = true;
				}
			if(t == 'f')
				{
				tf_index.push_back(index);
				tf_row.push_back(row);
				r = true;
				}
				
			//Add 2D box to quadtree for collision detection
			obj = new Object((float)(2*index), (float)(2*row), 1.5f, 1.5f);
			qt->AddObject(obj);

			glm::mat4 trans = glm::translate(2*(float)index, 0.f, 2*(float)row);

			vector<int>::iterator iit;
			for(iit = mesh->GetIndices()->begin(); iit != mesh->GetIndices()->end(); iit++)
			{
				indici->push_back(vertice_base+*iit);
			}

			vector<vertex>::iterator vit;
			for(vit = mesh->GetVertices()->begin(); vit != mesh->GetVertices()->end(); vit++ )
			{
				glm::vec4 dot(vit->position.x,vit->position.y,vit->position.z,vit->position.w);
				dot = trans*dot;
				float4 *pos = new float4(dot.x,dot.y,dot.z,dot.w);
				vertex *vert = new vertex(*pos,vit->normal,vit->t,vit->v);
				if(max_x < dot.x) max_x = dot.x;
				if(max_z < dot.z) max_z = dot.z;
				vertici->push_back(*vert);
				++vertice_base;
			}
		}
		// save starting position of the labyrinth
		if(r)
		{
			s_index = (ts_index.front() + ts_index.back())/2;
			s_row = (ts_row.front() + ts_row.back())/2;
			f_index = (tf_index.front() + tf_index.back())/2;
			f_row = (tf_row.front() + tf_row.back())/2;
			laby_width = index; 
			laby_height = row;
		}
		
		Mesh *mess = new Mesh(*vertici,*indici);
		meshMan->Add(mess);
		matMan->Add(mat);
		mess->Initialize();
		mat->LoadTexture(std::string("textures\\metal_box.jpg"));
		mat->LoadNormalMap(std::string("textures\\metal_box_N.jpg"));

		Model *pModel = new  Model(string("muro"),mess,mat);
		mModelMap.insert(std::pair<std::string,Model*>(string("muro"),pModel));
		mVect.push_back(pModel);
		pModel->Initialize();
		pModel->SetPosition(glm::vec3(0,0,0));

		ShaderManager::get_instance()->UseProgram(mat->GetShaderProgram());
		mat->UseProgram();
		ShaderManager::get_instance()->SetProjection(glm::perspective(60.0f,4.0f/3.0f,0.1f,1000.0f));
		ShaderManager::get_instance()->SetUniforms(glm::lookAt(glm::vec3(0,10,-20),glm::vec3(0,0,0),glm::vec3(0,1,0)));

		//LOAD FLOOR PLANE
		indici = new std::vector<int>();
		vertici = new std::vector<vertex>();
		vertice_base = 0;
		glm::mat4 trans = glm::translate(max_x/2, -(max_x/2.f+1.f), max_z/2);
		glm::mat4 scale = glm::scale(glm::vec3(max_x/2,max_x/2.f,max_z/2));
		glm::mat4 tot = trans * scale;
		vector<int>::iterator iit;

		for(iit = mesh->GetIndices()->begin(); iit != mesh->GetIndices()->end(); iit++)
		{
			indici->push_back(vertice_base+*iit);
		}
		vector<vertex>::iterator vit;
		for(vit = mesh->GetVertices()->begin(); vit != mesh->GetVertices()->end(); vit++ )
		{
			glm::vec4 dot(vit->position.x,vit->position.y,vit->position.z,vit->position.w);
			dot = tot*dot;
			float4 *pos = new float4(dot.x,dot.y,dot.z,dot.w);
			vertex *vert = new vertex(*pos,vit->normal,vit->t*max_x/2,vit->v*max_x/2);
			vertici->push_back(*vert);
			++vertice_base;
		}

		Material * floorMat = new Material(scene->mMaterials[0]);
		//floorMat->LoadTexture("textures\\grass1_ac.jpg");
		//floorMat->LoadNormalMap(std::string("textures\\grass1_ac_n.jpg"));
		floorMat->LoadTexture("textures\\asphalt_01.jpg");
		floorMat->LoadNormalMap(std::string("textures\\asphalt_01_N.jpg"));
		matMan->Add(floorMat);
		Mesh * floorMesh = new Mesh(*vertici,*indici);
		floorMesh->Initialize();
		meshMan->Add(floorMesh);
		Model * floorModel = new Model(string("floor"),floorMesh,floorMat);
		mModelMap.insert(std::pair<std::string,Model*>(string("floor"),floorModel));
		floorModel->Initialize();
		floorModel->SetPosition(glm::vec3(0,0,0));
		mVect.push_back(floorModel);
		ShaderManager::get_instance()->UseProgram(floorMat->GetShaderProgram());
		floorMat->UseProgram();
		ShaderManager::get_instance()->SetProjection(glm::perspective(60.0f,4.0f/3.0f,0.1f,1000.0f));
		ShaderManager::get_instance()->SetUniforms(glm::lookAt(glm::vec3(0,10,-20),glm::vec3(0,0,0),glm::vec3(0,1,0)));

		indici->clear();
		vertici->clear();
		for(iit = mesh->GetIndices()->begin(); iit != mesh->GetIndices()->end(); iit++)
			{
			indici->push_back(vertice_base+*iit);
			}
		for(vit = mesh->GetVertices()->begin(); vit != mesh->GetVertices()->end(); vit++ )
			{
			glm::vec4 dot(vit->position.x,vit->position.y,vit->position.z,vit->position.w);
			//			dot = dot;
			float4 *pos = new float4(dot.x,dot.y,dot.z,dot.w);
			vertex *vert = new vertex(*pos,vit->normal,vit->t*max_x,vit->v*max_x);
			vertici->push_back(*vert);
			++vertice_base;
			}
		/*Mesh * guiMesh = new Mesh(*vertici,*indici);
		guiMesh->Initialize();
		meshMan->Add(guiMesh);
		Material * guiMat = new Material();
		guiMat->LoadMaterial(std::string("shaders\\GUI.ps"),std::string("shaders\\GUI.vs"));
		guiMat->LoadTexture(string("textures\\grassy.jpg"));
		guiMat->LoadNormalMap(string("textures\\grassy_n.jpg"));
		guiMat->Initialize();
		matMan->Add(guiMat);
		Model * guiModel = new Model(string("gui"),guiMesh,guiMat);
		mModelMap.insert(std::pair<std::string,Model *>(string("gui"),guiModel));
		guiModel->Initialize();
		guiModel->SetPosition(glm::vec3(-5,0,3));
		mVect.push_back(guiModel);
		ShaderManager::get_instance()->UseProgram(guiMat->GetShaderProgram());
		guiMat->UseProgram();
		glm::mat4 orthoo = glm::ortho(0.f,(float)windowWidth,(float)windowHeight,0.f,0.f,100.f);
		int matprog =ShaderManager::get_instance()->GetProgram(guiMat->GetShaderProgram()); 
		glUseProgram(matprog);
		int loc = glGetUniformLocation(matprog,"u_projection");
		//glUniformMatrix4fv(loc,1,GL_FALSE,glm::value_ptr(orthoo));
		ShaderManager::get_instance()->SetProjection(glm::ortho(0.f,(float)windowWidth,(float)windowHeight,0.f,0.1f,110.f));
		//ShaderManager::get_instance()->SetProjection(glm::perspective(60.0f,4.0f/3.0f,0.1f,1000.0f));
		ShaderManager::get_instance()->SetUniforms(glm::lookAt(glm::vec3(0,0,0),glm::vec3(0,0,0),glm::vec3(0,1,0)));
		*/
		//mesh for skybox;
		Mesh * skyMesh = new Mesh(scene->mMeshes[0]);
		skyMesh->Initialize();
		skybox->m_pMesh = skyMesh;


		//bake shadow map
		ShadowMap->SetDimensions(784,562);
		ShadowMap->Init();
		ShadowMap->SetLightPosition(81,20,3);
		ShadowMap->SetLightDirection(81,0,3);
		ShadowMap->SetStaticMesh(mess);

		ShaderManager::get_instance()->SetLightsUniform(&lightMan.GetLights());
	} 
	else 
	{
		exit(1);
	}

}

void SceneManager::ElaborateScene2(void)
{
	//player
	Mesh * player = new Mesh(scene->mMeshes[0]);
	player->Initialize();
	meshMan->Add(player);
	Material * plMat = new Material();
	plMat->LoadMaterial(std::string("shaders\\default.ps"),std::string("shaders\\default.vs"));
	plMat->LoadTexture(string("textures\\tipo_texture.png"));
	plMat->LoadNormalMap(string("textures\\tipo_texture.png"));
	plMat->Initialize();
	matMan->Add(plMat);
	Model * plModel = new Model(string("player"),player,plMat);
	mModelMap.insert(std::pair<std::string,Model *>(string("player"),plModel));
	plModel->Initialize();
	plModel->SetPosition(glm::vec3(81,1,0));
	plModel->SetScale(glm::vec3(0.2,0.2,0.2));
	//plModel->SetRotation(glm::vec3(-90,0,0));
	mVect.push_back(plModel);
	ShaderManager::get_instance()->UseProgram(plMat->GetShaderProgram());
	plMat->UseProgram();
	ShaderManager::get_instance()->SetProjection(glm::perspective(60.0f,4.0f/3.0f,0.1f,1000.0f));
	ShaderManager::get_instance()->SetUniforms(glm::lookAt(glm::vec3(0,10,-20),glm::vec3(0,0,0),glm::vec3(0,1,0)));
}


std::vector<Model *> * SceneManager::GetModelsVector()
{
	return &(this->mVect);
}

std::map<std::string,Model*> * SceneManager::GetModels()
{
	return &(this->mModelMap);
}

SceneManager::~SceneManager(void)
{
	delete skybox;
	qt->Clear();
	delete(qt);
	std::map<std::string,Model*>::iterator it;
	for ( it=mModelMap.begin() ; it != mModelMap.end(); it++ )
	{
		//delete it->second;
	}
}


void SceneManager::Update(void)
{
	time.Update();
	/*glm::vec3 pos;
	int k=0;
	for (k=0;k<mVect.size();k++)
	{
	pos = mVect[k]->GetPosition();
	if(pos.y > 0)
	{
	pos.y = 0;
	mVect[k]->SetPosition(pos);
	}
	}*/
}

void SceneManager::Render(void)
{
	int k=0;
	for (k=0;k<mVect.size();k++)
	{
		/*if(BufferManager::get_instance()->BoundVertexVBO != mVect[k]->GetMesh()->GetVBO()){
			BufferManager::get_instance()->BindVBO(mVect[k]->GetMesh()->GetVBO(),mVect[k]->GetMesh()->GetVBO()+1);
		}*/
//		BufferManager::get_instance()->BindVBO(mVect[k]->GetMesh()->GetVBO(),mVect[k]->GetMesh()->GetVBO()+1);
		mVect[k]->Draw();
	}
	skybox->SetActive();
	skybox->SetModel(glm::vec3(cam->GetPosition().x,cam->GetPosition().y,cam->GetPosition().z));
	skybox->SetView(cam->GetViewMatrix());
	skybox->SetProjection(glm::perspective(60.0f,800.f/600.f,0.1f,1000.0f));
	skybox->Render();
}

Mesh * SceneManager::GetLabyrinth()
{
	return mVect.at(0)->GetMesh();
}