#pragma once
#include "Model.h"
#include "CTime.h" 
namespace flee{
	class Turret
	{
	public:
		Turret(void);
		~Turret(void);
		void Update(void);
		void Draw(void);
		Model * turretModel;
	private:
		float visualLenght,visualWidth;
		mytimer timer;
	};

};