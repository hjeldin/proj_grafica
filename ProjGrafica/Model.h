#pragma once
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include "defs.h"
#include "Material.h"
#include "Mesh.h"
#include "AABox.h"
#include "Vec3.h"
namespace flee{
	class Model
	{
	public:
		Model(void);
		Model(std::string name);
		Model(std::string name, aiMesh * mesh);
		Model(std::string name, aiMesh * mesh, aiMaterial * material);
		Model(std::string name, aiMesh * mesh, Material * material);
		Model(std::string name, Mesh * mesh, Material * material);
		~Model(void);
		void Translate(glm::vec3 t, float msec, float rX, float rY, float rZ);
		GLuint Initialize(void);
		void Shutdown(void);

		void SetPosition(glm::vec3 pos){ 
			this->x = pos.x;
			this->y = pos.y;
			this->z = pos.z;
			bbox.x = pos.x;
			bbox.y = pos.y;
			bbox.z = pos.z;
			this->width = 50;
			this->height = 50;
			this->position = pos; RecalcModelMatrix();
		}
		void SetRotation(glm::vec3 rot){ 
			this->rotation = rot; RecalcModelMatrix();
		}
		void SetScale(glm::vec3 scale){ 
			this->scale = scale; RecalcModelMatrix();
		}

		glm::vec3 GetPosition()
		{
			return this->position;
		}

		Mesh * GetMesh()
		{
			return this->mesh;
		}

		void Rotate(float, float, float);
		bool visible;
		void RecalcModelMatrix(void)
		{
			//scale * rotation * translation
			//glm::mat4 r = glm::mat4();
			glm::mat4 r = glm::rotate(glm::mat4(),rotation.x,glm::vec3(1,0,0));
			r = glm::rotate(r,rotation.y,glm::vec3(0,1,0));
			r = glm::rotate(r,rotation.z,glm::vec3(0,0,1));
			glm::mat4 t = glm::translate(glm::mat4(),position);
			glm::mat4 s = glm::scale(scale);
			modelMatrix = t * r * s;
			shaderMan->SetUniforms(shaderProgram,modelMatrix);
		}
		void Draw(void);

		void CheckErrors(void ){
#if DEBUG
			GLenum a = glGetError();
			if(a != GL_NO_ERROR){
				std::cout << glewGetErrorString(a) << "from model.h"<< std::endl;
			}
#endif
		}

		int x,y,z,width,height;
		AABox bbox;
	private:
		float rotationX,rotationY,rotationZ;
		std::string name;
		bool isLoaded;
		Material * mat;
		Mesh * mesh;
		unsigned int indices;	
		glm::vec3 position;
		glm::vec3 rotation;
		glm::vec3 scale;
		glm::mat4x4 modelMatrix;
		glm::mat4x4 viewMatrix;
		GLuint modelUniformLocation;
		GLuint shaderProgram;
		ShaderManager * shaderMan;
	};

}