#include "stdafx.h"
#include "defs.h"
#include "MeshManager.h"

using namespace flee;

MeshManager * MeshManager::instance_ptr = NULL;

MeshManager::MeshManager(void)
{
}


MeshManager::~MeshManager(void)
{
}

void MeshManager::Add(Mesh * mat)
{
	std::map<std::string, Mesh *>::iterator it;
	it = meshMap.end();
	int matUid = reinterpret_cast<int>(mat);
	meshMap.insert(it,std::map<std::string, Mesh *>::value_type(std::string(itoa_cpp(matUid,10)),mat));
}

void MeshManager::Remove(Mesh * mat)
{
	std::map<std::string, Mesh *>::iterator it;
	it = meshMap.begin();
	int matUid = reinterpret_cast<int>(mat);
	meshMap.erase(itoa_cpp(matUid,10));
}

void MeshManager::Purge()
{
	meshMap.erase(meshMap.begin(), meshMap.end());
}

Mesh * MeshManager::Get(std::string &str)
{
	return meshMap.find(str)->second;
}