#pragma once
#include "assimp.hpp"      // C++ importer interface
#include "aiScene.h"       // Output data structure
#include "aiPostProcess.h" // Post processing flags
#include <map>
#include "Model.h"
#include "MaterialManager.h"
#include "MeshManager.h"
#include "Quadtree.h"
#include "CTime.h"
#include "Camera.h"
#include <fstream>
#include <sstream>
#include "SkyBox.h"
#include "Object.h"
#include "ThirdPersonCamera.h"
#include "StaticShadowMap.h"
#include "LightManager.h" 

namespace flee{
	/**
		Manages every object in current scene.
		SceneManager is responsable for update and render every model on screen.
		Should also check for collisions and so...
		Ultra buggy.
	*/
	class SceneManager
		{
		public:
			SceneManager(void);										///default constructor
			void Init(void);										///Initialize scene manager
			~SceneManager(void);									///default destructor
			StaticShadowMap * ShadowMap;							///pointer to ShadowMap
			bool ImportAsset( const std::string& pFile);			///imports specified file into scene. Actually the specified object is the only taken in account when creating the scene. Shold be fixed
			void ElaborateScene();									///Elaborates scene from input files \warning CHECK!!!!!!!
			std::map<std::string,Model*> * GetModels();				/** Getter for models /return a model map */
			void Render(void);										///Default method for render every model
			void SetWindow(int w, int h);							///Buggy method to handle window resize.
			Mesh * GetLabyrinth();
			/**
				\warning SetCamera should be checked for consistency and be moved on cpp file
			*/
			inline void SetCamera(Camera * cam){
				//mVect.at(mVect.size()-1)->visible = false;
				cam->Follow(mVect.at(mVect.size()-1));
					this->cam = cam;
			}
			void Update(void);										///Default method to update every model
			float getSIndex()										///\warning getSIndex(), getSRow() etc should be checked and, if necessary, moved on cpp file
				{
				return s_index;
				}
			float getSRow()											///\warning getSIndex(), getSRow() etc should be checked and, if necessary, moved on cpp file
				{
				return s_row;
				}
			float getFIndex()										///\warning getSIndex(), getSRow() etc should be checked and, if necessary, moved on cpp file
				{
				return f_index;
				}
			float getFRow()											///\warning getSIndex(), getSRow() etc should be checked and, if necessary, moved on cpp file
				{
				return f_row;
				}
			Quadtree* getQuadTree()									///\warning getSIndex(), getSRow() etc should be checked and, if necessary, moved on cpp file
				{
				return qt;
				}
			float getLWidth()										///\warning getSIndex(), getSRow() etc should be checked and, if necessary, moved on cpp file
				{
				return laby_width;
				}
			float getLHeight()										///\warning getSIndex(), getSRow() etc should be checked and, if necessary, moved on cpp file
				{
				return laby_height;
				}

			void RenderDebugTexture(GLuint texID);
			std::vector<Model *> * GetModelsVector();				///Getter for models \return a vector of models
			void ElaborateScene2(void);
		private:
			LightManager lightMan;									///Lights manager
			mytimer time;											///Sync timer 
			GLuint currentShaderProgram;							///\warning ? dafuq ?
			GLuint projectionUniformLocation;						///\warning ? dafuq ?
			int width,height;										///\warning ? dafuq ?
			std::map<std::string,Model*> mModelMap;					///Model map
			const aiScene* scene;									///Scene pointer to culoimport
			Assimp::Importer importer;								///Importer?WAT? 
			MaterialManager * matMan;								///HUMAN?! STHAP! WHAT R U DOING?! STHAP!!!
			MeshManager * meshMan;									///Pointer to mesh manager
			std::vector<Model*> mVect;								///\warning ? dafuq ?
			Quadtree * qt;											///Quadtree pointer used for collision detection \warning buggy
			Object * obj;											///\warning ? DAFUQ IS AN OBJECT ??????
			Camera * cam;											///Pointer to camera
			SkyBox *skybox;											///Pointer to skybox
			float s_index, s_row, f_index, f_row;					///DUNNO!
			int laby_width, laby_height;							///DUNNO!			
		};

	}