#include "StdAfx.h"
#include "Mesh.h"


using namespace flee;
Mesh::Mesh(void)
{
	bManager = BufferManager::get_instance();
	isLoaded = false;
	vertices.push_back(vertex(float4(1.0,1.0,1.0,1.0f),float4(1.0,1.0,1.0,1.0f),float3(1.0,1.0,1.0)));
	indices.push_back(0);
	isLoaded = true;
}

Mesh::Mesh(aiMesh * pMesh)
{
	bManager = BufferManager::get_instance();
	isLoaded = false;
	this->mMesh = pMesh;
}

Mesh::Mesh(std::vector<vertex> vertices, std::vector<int> indices)
{
	bManager = BufferManager::get_instance();
	isLoaded = false;
	this->vertices = vertices;
	this->indices = indices;
}


Mesh::~Mesh(void)
{
}

void Mesh::Initialize(void)
{
	if(this->vertices.size() != 0)
		this->InitializeBuffersVertex();
	else 
		this->InitializeBuffers();

	mVAO = bManager->Add(this);
	mVBO = bManager->BoundVertexVBO;
	isLoaded = true;
}

void Mesh::Shutdown(void)
{
	bManager->Remove(this);
}


void Mesh::Draw(void)
{
	//bManager->BindVBO(this->mVBO,this->mVBO+1);
	if((void *)glDrawElementsBaseVertex)
		glDrawElementsBaseVertex(GL_TRIANGLES, this->m_indexCount, GL_UNSIGNED_INT, (void *)(this->indexOffset),this->vertexOffset);
}

void Mesh::SetIOffset(unsigned int off)
{
	std::cout << "indices : " << indices.size() << std::endl;
	this->indexOffset = off;
}

void Mesh::SetVOffset(unsigned int off)
{
	std::cout << "vertices : " << vertices.size() << std::endl;
	this->vertexOffset = off;
}

void Mesh::InitializeBuffersVertex(void)
{
	m_vertexCount = vertices.size();
	m_indexCount = indices.size();
}

void Mesh::InitializeBuffers(void)
{
	unsigned long int i = 0;
	m_vertexCount = 0;
	m_indexCount = 0;

	m_vertexCount = mMesh->mNumVertices;
	m_indexCount = mMesh->mNumFaces*3;

	if(m_vertexCount <= 0) return;
	if(m_indexCount <= 0) return;
	//vertices.resize(m_vertexCount);
	indices.resize(m_indexCount);

	for(i=0;i<m_vertexCount;i++)
	{
		vertex tmp;
		tmp.position = float4(mMesh->mVertices[i].x,mMesh->mVertices[i].y,mMesh->mVertices[i].z,1.0f);
		if(mMesh->HasVertexColors(i)){
			tmp.color = float4(mMesh->mColors[i]->r,mMesh->mColors[i]->g,mMesh->mColors[i]->b,1.0f);
		}else{
			tmp.color = float4(1.0f,1.0f,1.0f,1.0f);
		}
		if(mMesh->HasNormals()){
			tmp.normal = float3(mMesh->mNormals[i].x,mMesh->mNormals[i].y,mMesh->mNormals[i].z);
		}
		if(mMesh->HasTextureCoords(0))
		{
			tmp.t = mMesh->mTextureCoords[0][i].x;
			tmp.v = mMesh->mTextureCoords[0][i].y;
		}
		//vertices[i].fv = scene->mMeshes[0]->mTextureCoords
		vertices.push_back(tmp);
	}

	/*
	for(i = 0; i<8;i++)
	{
		vertices.push_back(vertex());
	}
	// Load the vertex array with data.
	vertices[0].position = float4(1.0f,-1.0f,1.0f,1.0f);  // Bottom left.
	vertices[0].color = float4(0.0f, 1.0f, 0.0f, 1.0f);

	vertices[1].position = float4( -1.0f,-1.0f,  1.0f,1.0f);  // Top middle.
	vertices[1].color = float4(1.0f, 1.0f, 0.0f, 1.0f);

	vertices[2].position = float4( -1.0f, 1.0f,  1.0f,1.0f);  // Bottom right.
	vertices[2].color = float4(0.0f, 1.0f, 1.0f, 1.0f);

	vertices[3].position = float4( 1.0f, 1.0f,  1.0f,1.0f);  // Bottom right.
	vertices[3].color = float4(1.0f, 0.0f, 0.0f, 1.0f);
	
	vertices[4].position = float4(1.0f, 1.0f, -1.0f ,1.0f);  // Bottom right.
	vertices[4].color = float4(0.0f, 0.0f, 0.0f, 1.0f);

	vertices[5].position = float4( -1.0f, 1.0f, -1.0f,1.0f);  // Bottom right.
	vertices[5].color = float4(1.0f, 1.0f, 1.0f, 1.0f);

	vertices[6].position = float4( -1.0f, -1.0f, -1.0f,1.0f);  // Bottom right.
	vertices[6].color = float4(1.0f, 1.0f, 1.0f, 1.0f);

	vertices[7].position = float4( 1.0f,  -1.0f, -1.0f,1.0f);  // Bottom right.
	vertices[7].color = float4(0.0f, 0.0f, 1.0f, 1.0f);

	unsigned long g_Indices[36] = {
		//7,5,4,6,5,7,1,0,7,7,6,1,7,0,3,3,4,7,1,6,2,2,6,5,5,4,3,3,2,5,0,1,2,2,3,0
		//7,4,5,6,7,5,1,0,7,7,6,1,7,0,3,3,4,7,1,6,2,2,6,5,5,4,3,3,2,5,0,1,2,2,3,0
		//0,1,2,2,3,0,7,0,3,3,4,7,7,4,5,5,6,7,1,0,7,7,6,1,6,5,2,2,1,6,5,4,3,3,2,6
		//0,2,1,0,3,2,7,3,0,3,7,4,7,5,4,5,7,6,1,7,0,7,1,6,6,5,2,2,6,1,5,3,4,3,5,2
		0,1,2,2,3,0,0,3,7,7,3,4,7,4,5,5,6,7,7,6,1,1,0,7,4,3,2,2,5,4,6,5,2,2,1,6
	};
	for(i = 0;i < 36;i++)
	{
		indices[i] = g_Indices[i];
	}*/
	
	unsigned int count = 0;
	for(i = 0; i < mMesh->mNumFaces; i++)
	{
		indices[count++] = mMesh->mFaces[i].mIndices[0];
		indices[count++] = mMesh->mFaces[i].mIndices[1];
		indices[count++]= mMesh->mFaces[i].mIndices[2];
	}
	

}
