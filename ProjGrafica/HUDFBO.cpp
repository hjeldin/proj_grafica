#include "stdafx.h"
#include "HUDFBO.h"

using namespace flee;
HUDFBO::HUDFBO(void)
{
	m_fbo = 0;
    m_HUD = 0;
	this->shaders[0] = std::string("");
	this->shaders[1] = std::string("");
}


HUDFBO::~HUDFBO(void)
{
	glUseProgram(this->shaderProgram);
	glDeleteShader(this->mVs);
	glDeleteShader(this->mFs);
	glDeleteProgram(this->shaderProgram);
	if (m_fbo != 0) {
        glDeleteFramebuffers(1, &m_fbo);
    }

    if (m_HUD != 0) {
        glDeleteTextures(1, &m_HUD);
    }
}


bool HUDFBO::Init()
{
	try{
		this->shaders[0] = file2string(std::string("shaders\\HUD.ps"));
		this->shaders[1] = file2string(std::string("shaders\\HUD.vs"));
		mFs = ShaderManager::get_instance()->CompileShaderOfType(GL_FRAGMENT_SHADER, this->shaders[0]);
		mVs = ShaderManager::get_instance()->CompileShaderOfType(GL_VERTEX_SHADER, this->shaders[1]);
		this->shaderProgram = ShaderManager::get_instance()->LinkShaders(mVs,mFs,0);
		this->shaderProgram = ShaderManager::get_instance()->GetProgram(this->shaderProgram);
		glUseProgram(this->shaderProgram);
		modelMatrix = glGetUniformLocation(shaderProgram, "u_model");
		viewMatrix = glGetUniformLocation(shaderProgram, "u_view");
		projectionMatrix = glGetUniformLocation(shaderProgram, "u_projection");
		//LoadTexture(std::string("textures\\crate_tex2.jpg"))
	}catch(char * str)
	{
		std::cout << str << std::endl;
	}
    // Create the FBO
    glGenFramebuffers(1, &m_fbo);    

    // Create the texture object
    glGenTextures(1, &m_HUD);
    glBindTexture(GL_TEXTURE_2D, m_HUD);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 800, 800, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	//glGenerateMipmap(GL_TEXTURE_2D);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_fbo);
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_HUD, 0);

    GLenum Status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if (Status != GL_FRAMEBUFFER_COMPLETE) {
        printf("FB error, status: 0x%x\n", Status);
        return false;
    }
	glm::mat4 orth = glm::ortho(0.0f,20.0f,20.0f,0.0f,0.1f,100.0f);
	SetProjection(orth);
	SetModel(glm::vec3(1,1,1));
    return true;
}

void HUDFBO::Render(Mesh * modelVec)
{
	glViewport(0,0,800,800);
	//In the first part we bind the framebuffer, activate the shaders, update them and render to a texture
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_fbo);
	glUseProgram(this->shaderProgram);
	glClearColor(1,1,1,1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	modelVec->Draw();
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER,0);
	glViewport(0,0,(int)windowWidth,(int)windowHeight);
	//Here we bind the render-to-quad shaders and render on it
	glUseProgram(this->program);
	glBindTexture(GL_TEXTURE_2D,m_HUD);
	glUniform1i(texLocation, 0); 
	glUniform4f(centerPoint,-0.75f,0.65f,0.2f,0.2f);
	glDrawArrays(GL_POINTS, 0, 1);
}

void HUDFBO::SetProjection(glm::mat4x4 proj)
{
	
	glUniformMatrix4fv(projectionMatrix,1,GL_FALSE,glm::value_ptr(proj));
}
void HUDFBO::SetView(glm::mat4x4 view)
{
	glUseProgram(this->shaderProgram);
	glUniformMatrix4fv(viewMatrix,1,GL_FALSE,glm::value_ptr(view));
}
void HUDFBO::SetModel(glm::vec3 model)
{
	glm::mat4 modelM = glm::translate(glm::mat4(),model);
	glUniformMatrix4fv(modelMatrix,1,GL_FALSE,glm::value_ptr(modelM));
}