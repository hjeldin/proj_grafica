#include "stdafx.h"
#include "StaticShadowMap.h"

using namespace flee;

StaticShadowMap::StaticShadowMap(void)
{
}


StaticShadowMap::~StaticShadowMap(void)
{
}

void StaticShadowMap::Init(void)
{
	AttachShader();
	GenerateFBO();
}

void StaticShadowMap::SetDimensions(int w ,int h)
{
	width = w;
	height = h;
}

void StaticShadowMap::AttachShader(void)
{

	std::ifstream t( "shaders\\shadowmap.ps" );
	std::string str;

	t.seekg( 0, std::ios::end );
	str = std::string( t.tellg(), ' ' );
	t.seekg( 0, std::ios::beg );

	str.assign( ( std::istreambuf_iterator<char>( t ) ), std::istreambuf_iterator<char>() );
	
	pShader = ShaderManager::get_instance()->CompileShaderOfType(GL_FRAGMENT_SHADER,str);
	
	
	t = std::ifstream("shaders\\shadowmap.vs" );

	t.seekg( 0, std::ios::end );
	str = std::string( t.tellg(), ' ' );
	t.seekg( 0, std::ios::beg );

	str.assign( ( std::istreambuf_iterator<char>( t ) ), std::istreambuf_iterator<char>() );
	vShader = ShaderManager::get_instance()->CompileShaderOfType(GL_VERTEX_SHADER,str);
	program = ShaderManager::get_instance()->GetProgram(ShaderManager::get_instance()->LinkShaders(vShader,pShader,0));
	glUseProgram(program);
	glm::mat4 proj = glm::perspective(45.f,(float)windowWidth/(float)windowHeight,7.0f,1000.0f);
	GLuint projectionM = glGetUniformLocation(program,"u_projection");
	glUniformMatrix4fv(projectionM,1,false,glm::value_ptr(proj));
	this->projection = proj;
	GLuint modelM = glGetUniformLocation(program,"u_model");
	glUniformMatrix4fv(modelM,1,false,glm::value_ptr(glm::mat4(1)));
	this->model = glm::mat4(1);
}

void StaticShadowMap::GenerateFBO(void)
{
	glGenFramebuffers(1,&fboID);
	// Create the depth buffer
	glGenTextures(1, &texID);
	//glGenTextures (1, &colorTexID);

	glBindFramebuffer(GL_FRAMEBUFFER, fboID);
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, 2048, 2048, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glDrawBuffer(GL_NONE);
	//glTexImage2D (GL_TEXTURE_2D,0, GL_RGBA, 2048, 2048, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	//glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorTexID, 0);

	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texID,0);
	//glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorTexID,0);

	/*glBindRenderbuffer(GL_RENDERBUFFER,depthID);
	glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT24,GL_RENDERBUFFER,depthID);*/
	// Disable writes to the color buffer

	GLenum Status = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);

	if (Status != GL_FRAMEBUFFER_COMPLETE) {
		std::cout << "FB error, status: " << Status << ". COULD NOT INITIALIZE STATIC SHADOWS" << std::endl;
	}


}
void StaticShadowMap::Render(void)
{
	glCullFace(GL_FRONT);
	glViewport(0,0,2048,2048);
	glUseProgram(program);
	if(glGetError() != GL_NO_ERROR)
	{
		std::cout << "motherfucking error" << std::endl;
	}
	if(glGetError() != GL_NO_ERROR)
	{
		std::cout << "motherfucking error" << std::endl;
	}
	glBindTexture(GL_TEXTURE_2D,0);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fboID);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	toDraw->Draw();
	if(glGetError() != GL_NO_ERROR)
	{
		std::cout << "motherfucking error" << std::endl;
	}
	//SOMETHING MAGICAL STOPS HERE
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glCullFace(GL_BACK);
	glViewport(0,0,(float)windowWidth,(float)windowHeight);
	
}
void StaticShadowMap::Deinit(void)
{
	glUseProgram(program);
	glDeleteTextures(1,&texID);
	glDeleteShader(vShader);
	glDeleteShader(pShader);
	glDeleteProgram(program);
	glDeleteFramebuffers(1,&fboID);
}
GLuint StaticShadowMap::GetShadowTexture(void)
{
	return texID;
}
void StaticShadowMap::SetLightPosition(int x,int y ,int z)
{
	lightpos.x = x;
	lightpos.y = y;
	lightpos.z = z;
}

void StaticShadowMap::SetLightDirection(int x, int y, int z)
{
	lightdir.x = x;
	lightdir.y = y;
	lightdir.z = z;
	//Once we have both position and direction we can update the uniform
	//glm::mat4 viewMat = glm::lookAt(lightpos.toVec3(),lightdir.toVec3(),glm::vec3(0,1,0));
	glm::mat4 viewMat = glm::lookAt(lightpos.toVec3(),lightdir.toVec3(),glm::vec3(0,0,1));
	view = viewMat;
	GLuint view  = glGetUniformLocation(program,"u_view");
	glUniformMatrix4fv(view,1,false,glm::value_ptr(viewMat));
}

void StaticShadowMap::SetStaticMesh(Mesh * m)
{
	this->toDraw = m;
}

glm::mat4 StaticShadowMap::GetShadowMVP()
{
	glm::mat4 biasMatrix(0.5, 0.0, 0.0, 0.0,0.0, 0.5, 0.0, 0.0,0.0, 0.0, 0.5, 0.0,0.5, 0.5, 0.5, 1.0);
	glm::mat4 mmm = projection * view * model;
	glm::mat4 depthBiasMVP =biasMatrix  * mmm;
	//glm::mat4 depthBiasMVP = mmm;
	return depthBiasMVP;
}