#include "StdAfx.h"
#include "Material.h"
#include "BufferManager.h"
#include "IL\il.h"
#include "IL\ilu.h"
using namespace flee;

Material::Material(void)
{
	this->ambientIntensity = 0.1f;
	this->smooth = false;
	this->ambientColor = float4(1.0f,1.0f,1.0f,1.0f) * ambientIntensity;
	this->diffuseColor = float4(1.0f,0.0f,0.0f,1.0f);
	this->emissiveColor = float4(1.0f,1.0f,1.0f,1.0f);
	this->specularColor = float4(1.0f,1.0f,1.0f,1.0f);
	this->shaders[0] = std::string("");
	this->shaders[1] = std::string("");
	this->shaders[2] = std::string("");
}

Material::Material(aiMaterial * material)
{
	this->mat = material;
	try{
		this->shaders[0] = file2string(std::string("shaders\\default.ps"));
		this->shaders[1] = file2string(std::string("shaders\\default.vs"));
		mFs = ShaderManager::get_instance()->CompileShaderOfType(GL_FRAGMENT_SHADER, this->shaders[0]);
		mVs = ShaderManager::get_instance()->CompileShaderOfType(GL_VERTEX_SHADER, this->shaders[1]);
		this->shaderProgram = ShaderManager::get_instance()->LinkShaders(mVs,mFs,0);
		ShaderManager::get_instance()->GetUniforms(this->shaderProgram);
		diffuseLoc = glGetUniformLocation(ShaderManager::get_instance()->GetProgram(this->shaderProgram), "diffuseTex");
		normalLoc =  glGetUniformLocation(ShaderManager::get_instance()->GetProgram(this->shaderProgram), "normalTex");
		shadowLoc = glGetUniformLocation(ShaderManager::get_instance()->GetProgram(this->shaderProgram), "shadowTex");
		CheckError();
	}catch(char * str)
	{
		std::cout << str << std::endl;
	}
	//this->shaders[2] = file2string(std::string("shaders\\default.gs"));
	std::cout << "shaders loaded" << std::endl;
}

void Material::Initialize(void)
{
	/*aiColor3D ambient;
	mat->Get("AI_MATKEY_COLOR_AMBIENT",0,0,ambient);
	this->ambientColor = float4(ambient.r,ambient.g,ambient.b,1.0f);
	aiColor3D diffuse;
	mat->Get("AI_MATKEY_COLOR_DIFFUSE",0,0,diffuse);
	this->ambientColor = float4(diffuse.r,diffuse.g,diffuse.b,1.0f);
	aiColor3D emissive;
	mat->Get("AI_MATKEY_COLOR_EMISSIVE",0,0,emissive);
	this->ambientColor = float4(emissive.r,emissive.g,emissive.b,1.0f);
	aiColor3D specular;
	mat->Get("AI_MATKEY_COLOR_SPECULAR",0,0,ambient);
	this->ambientColor = float4(specular.r,specular.g,specular.b,1.0f);
	int shade;
	mat->Get("AI_MATKEY_SHADING_MODEL",0,0,shade);
	this->smooth = shade;*/
	this->ambientColor = float4(1.0f,1.0f,1.0f,1.0f);
	this->ambientColor = float4(1.0f,1.0f,1.0f,1.0f);
	this->ambientColor = float4(1.0f,1.0f,1.0f,1.0f);
	this->ambientColor = float4(1.0f,1.0f,1.0f,1.0f);
	this->ambientColor = float4(1.0f,1.0f,1.0f,1.0f);
}

Material::~Material(void)
{
	this->Shutdown();
}

void Material::UseProgram()
{
	ShaderManager::get_instance()->UseProgram(this->shaderProgram);
	int k = ShaderManager::get_instance()->GetProgram(this->shaderProgram);
	glActiveTexture(GL_TEXTURE0);
	if(shadowLoc != (GLuint)-1)
		glUniform1i(shadowLoc, 0); 
	glBindTexture(GL_TEXTURE_2D,texture);
	glUniform1i(diffuseLoc, 0); 
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D,normalTex);
	glUniform1i(normalLoc,1);
	CheckError();
}

void Material::StopProgram(void)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0); 
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0); 
	glUseProgram(0);
}


std::string Material::file2string(std::string filename )
{
	std::ifstream t( filename );
	std::string str;

	t.seekg( 0, std::ios::end );
	str = std::string( t.tellg(), ' ' );
	t.seekg( 0, std::ios::beg );

	str.assign( ( std::istreambuf_iterator<char>( t ) ), std::istreambuf_iterator<char>() );
	return str;
}

void Material::LoadMaterial(std::string a, std::string b)
{
	this->shaders[0] = file2string(a);
	this->shaders[1] = file2string(b);
	mFs = ShaderManager::get_instance()->CompileShaderOfType(GL_FRAGMENT_SHADER, this->shaders[0]);
	mVs = ShaderManager::get_instance()->CompileShaderOfType(GL_VERTEX_SHADER, this->shaders[1]);
	this->shaderProgram = ShaderManager::get_instance()->LinkShaders(mVs,mFs,0);
	ShaderManager::get_instance()->GetUniforms(this->shaderProgram);
	diffuseLoc = glGetUniformLocation(ShaderManager::get_instance()->GetProgram(this->shaderProgram), "diffuseTex");
	normalLoc =  glGetUniformLocation(ShaderManager::get_instance()->GetProgram(this->shaderProgram), "normalTex");
	shadowLoc =  glGetUniformLocation(ShaderManager::get_instance()->GetProgram(this->shaderProgram), "shadowTex");
	CheckError();
}

void Material::Shutdown(void)
{
	glDeleteTextures(1, &texture);
	glDeleteTextures(1,&normalTex);
	ShaderManager::get_instance()->UseProgram(this->shaderProgram);
	glDeleteShader(mFs);
	glDeleteShader(mVs);
	glDeleteProgram(ShaderManager::get_instance()->GetProgram(this->shaderProgram));
}

bool Material::LoadTexture(std::string filename)
{
	bool success;
	ilGenImages(1, &texName); 
	ilBindImage(texName);
	char cCurrentPath[FILENAME_MAX];
	if (! _getcwd(cCurrentPath, sizeof(cCurrentPath) / sizeof(TCHAR))) 
	{ 
	return errno; 
	} 
 
	cCurrentPath[sizeof(cCurrentPath) - 1] = '\0'; /* not really required */ 
	std::string fullPath = std::string(cCurrentPath).append(std::string("\\").append(filename));
	success = ilLoadImage((wchar_t*)fullPath.c_str()); 
	if (success) /* If no error occured: */
	{
		success = ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE); /* Convert every colour component into
			unsigned byte. If your image contains alpha channel you can replace IL_RGB with IL_RGBA */
		if (!success)
		{
			/* Error occured */
			MessageBox(NULL,
				(LPCWSTR)L"Could not load texture",
				(LPCWSTR)L"Bye!",
				MB_ICONERROR| MB_OK | MB_DEFBUTTON2);
			exit(1);
			return false;
		}
		glGenTextures(1, &texture); /* Texture name generation */
		CheckError();
		glBindTexture(GL_TEXTURE_2D, texture); /* Binding of texture name */
		CheckError();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR); 
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); 
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		GLfloat fLargest;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fLargest);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, fLargest);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, ilGetInteger(IL_IMAGE_WIDTH),
			ilGetInteger(IL_IMAGE_HEIGHT), 0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE,
			ilGetData());
		glGenerateMipmap(GL_TEXTURE_2D); 
		if(texture == -1){
			MessageBox(NULL,
				(LPCWSTR)L"Could not load texture",
				(LPCWSTR)L"Bye!",
				MB_ICONERROR| MB_OK | MB_DEFBUTTON2);
			exit(1);
			return false;
		}
		CheckError();
	} else {
		MessageBox(NULL,
			(LPCWSTR)L"Could not load texture",
			(LPCWSTR)L"Bye!",
			MB_ICONERROR| MB_OK | MB_DEFBUTTON2);
		ILenum Error;
		while ((Error = ilGetError()) != IL_NO_ERROR) { 
			printf("%d: %s/n", Error, iluErrorString(Error)); 
		}
	}
	ilDeleteImages(1, &texName); 
	return false;
}

bool Material::LoadNormalMap(std::string filename)
{
	bool success;
	char cCurrentPath[FILENAME_MAX];
	if (! _getcwd(cCurrentPath, sizeof(cCurrentPath) / sizeof(TCHAR))) 
	{ 
	return errno; 
	} 
 
	cCurrentPath[sizeof(cCurrentPath) - 1] = '\0'; /* not really required */ 
	std::string fullPath = std::string(cCurrentPath).append(std::string("\\").append(filename));
	success = ilLoadImage((wchar_t*)filename.c_str()); 
	if (success) /* If no error occured: */
	{
		success = ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE); /* Convert every colour component into
			unsigned byte. If your image contains alpha channel you can replace IL_RGB with IL_RGBA */
		if (!success)
		{
			/* Error occured */
			MessageBox(NULL,
				(LPCWSTR)L"Could not load texture",
				(LPCWSTR)L"Bye!",
				MB_ICONERROR| MB_OK | MB_DEFBUTTON2);
			exit(1);
			return false;
		}
		glGenTextures(1, &normalTex); /* Texture name generation */
		glBindTexture(GL_TEXTURE_2D, normalTex); /* Binding of texture name */
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); 
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, ilGetInteger(IL_IMAGE_WIDTH),
			ilGetInteger(IL_IMAGE_HEIGHT), 0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE,
			ilGetData());
		glGenerateMipmap(GL_TEXTURE_2D);
		if(normalTex == -1){
			MessageBox(NULL,
				(LPCWSTR)L"Could not load texture",
				(LPCWSTR)L"Bye!",
				MB_ICONERROR| MB_OK | MB_DEFBUTTON2);
			exit(1);
			return false;
		}
		CheckError();
	} else {
		MessageBox(NULL,
			(LPCWSTR)L"Could not load texture",
			(LPCWSTR)L"Bye!",
			MB_ICONERROR| MB_OK | MB_DEFBUTTON2);
		ILenum Error;
		while ((Error = ilGetError()) != IL_NO_ERROR) { 
			printf("%d: %s/n", Error, iluErrorString(Error)); 
		}
	}
	ilDeleteImages(1, &texName); 
	return success;
}