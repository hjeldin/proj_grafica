#version 150
#extension ARB_explicit_attrib_location : require

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;
uniform mat4 u_shadowMVP;

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 colour;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec2 texPos;

smooth out vec3 normalV;
smooth out vec2 texPosV;

out vec4 colourV;
out vec3 posV;
out vec4 shadowCoord;
out mat4 modelV;

void main (void)
{
	mat4 mvMat = u_view * u_model;
	mat4 worldMatrix = u_projection * u_view * u_model;
	normalV = mat3(inverse(transpose(u_model))) * normal;
    colourV = colour;
	posV = vec3(u_model * vec4(position,1));
	texPosV = texPos;
	shadowCoord = u_shadowMVP * vec4(position,1);
	modelV = u_view;
	//Set vertex position
	gl_Position =  worldMatrix * vec4(position,1);
}