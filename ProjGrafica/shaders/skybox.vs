#version 330
layout (location = 0) in vec3 Position;
layout(location = 1) in vec4 colour;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec2 texPos;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;

out vec3 TexCoord0;
out vec3 normalV;
out vec3 colorV;

void main()
{
    vec4 WVP_Pos = u_projection * u_view *  u_model * vec4(Position, 1.0);
    gl_Position = WVP_Pos.xyww;
	vec3 p;
	p.x = Position.x;
	p.y = Position.y;
	p.z = Position.z;
    TexCoord0 = p;
	colorV = vec3(colour);
	normalV = normal;
} 