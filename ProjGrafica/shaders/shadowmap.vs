#version 150
#extension ARB_explicit_attrib_location : require
uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;
uniform mat3 u_inverse;
layout (location = 0) in vec3 Position;
layout (location = 1) in vec2 TexCoord;
layout (location = 2) in vec3 Normal;


out vec2 TexCoordOut;
out vec4 posv;
void main()
{
	mat4 worldMatrix = u_projection * u_view * u_model;
    gl_Position = worldMatrix * vec4(Position, 1.0);
	posv = gl_Position;
    TexCoordOut = TexCoord;
} 