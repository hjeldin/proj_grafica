#version 150
#extension ARB_explicit_attrib_location : require

uniform vec4 u_ambientColor;
uniform vec4 u_diffuseColor;
uniform vec4 u_emissiveColor;
uniform vec4 u_specularColor;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;
uniform mat3 u_inverse;

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 colour;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec2 texPos;

out vec2 texPosV;

void main (void)
{
	mat4 worldMatrix = u_projection * u_view * u_model;
	vec4 pos =  worldMatrix * vec4(position,1);
	gl_Position = vec4(pos.x,pos.y,2,1);
	texPosV = position.xy;
    //gl_Position =  pos;
}