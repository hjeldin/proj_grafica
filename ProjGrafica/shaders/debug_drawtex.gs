#version 150
#extension GL_EXT_geometry_shader3 : enable 

uniform vec4 centerPoint;

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

out vec2 texcoord;

void main() 
{
    gl_Position = vec4( centerPoint.x + centerPoint.z, centerPoint.y + centerPoint.w, 0.1, 1.0 );
    texcoord = vec2( 1.0, 1.0 );
    EmitVertex();

    gl_Position = vec4(centerPoint.x - centerPoint.z, centerPoint.y + centerPoint.w, 0.1, 1.0 );
    texcoord = vec2( 0.0, 1.0 ); 
    EmitVertex();

    gl_Position = vec4( centerPoint.x + centerPoint.z, centerPoint.y - centerPoint.w, 0.1, 1.0 );
    texcoord = vec2( 1.0, 0.0 ); 
    EmitVertex();

    gl_Position = vec4(centerPoint.x - centerPoint.w,centerPoint.y - centerPoint.w, 0.1, 1.0 );
    texcoord = vec2( 0.0, 0.0 ); 
    EmitVertex();

    EndPrimitive(); 
}
