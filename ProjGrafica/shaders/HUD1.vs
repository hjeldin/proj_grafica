#version 150
#extension ARB_explicit_attrib_location : require

uniform vec4 u_ambientColor;
uniform vec4 u_diffuseColor;
uniform vec4 u_emissiveColor;
uniform vec4 u_specularColor;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;
uniform mat3 u_inverse;

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 colour;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec2 texPos;

out vec2 texPosV;

void main (void)
{
	texPosV = position.xy*vec2(0.5,0.5)+vec2(0.5,0.5);
    gl_Position =  vec4(position.xy,0.0,1.0);
}