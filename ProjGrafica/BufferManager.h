#pragma once

#include "Mesh.h"
#include "defs.h"

namespace flee{
	class Mesh;
	class BufferManager
	{
	public:
		struct vao_str
		{
			float x,y,z,r,g,b,a,nx,ny,nz;
			float ts1,ts2,tr1,tr2,tq1,tq2;
			//float p1,p2,p3;
		};
		static BufferManager* get_instance()
		{
			if(instance_ptr == NULL){
				instance_ptr = new BufferManager();
			}
			return instance_ptr;
		}
		~BufferManager(void);
		unsigned int Add(Mesh * mesh);
		void Remove(Mesh * pMesh);
		void FillBuffers();
		int BoundVertexVBO,BoundIndexVBO;
	private:
		BufferManager(void);
		static BufferManager * instance_ptr;
		void CheckError(void);
		unsigned int GenerateNewVAO(void);
		int offsets;
		int vOffset;
		std::vector<Mesh *> meshes;
		std::vector<GLuint> * VBOs;				//array of vertex buffer object
		std::vector<GLuint> * VAOs;				//array of vertex array object
		std::vector<int> *occupiedVBOS;
		static const int maxVBufferSize = 4 * 1024 * 1024;
		static const int mVertexSize = sizeof(vertex);	//vertex size in bits
		//float4 position; -> float float float
		//float4 color; -> float float float float
		//float3 normal; -> float float float
		std::vector<vertex> * vertices;
		std::vector<int> * indices;
		GLuint newVao;
		GLuint newVbo[2];
	};
}
