#include "stdafx.h"
#include "WinApplication.h"

using namespace flee;

WinApplication::WinApplication(void):hasFocus(true),running(true),draw(true),generatedShadows(false)
{
}


WinApplication::~WinApplication(void)
{

}


void WinApplication::Init(void)
{
	MaterialManager::get_instance();
	sceneMan = new SceneManager();
	timer.Initialize();
	context = new WinOGLContext();
	bool a,b,c;
	context->SetHandle(hWnd);
	context->windowWidth = windowWidth;
	context->windowHeight = windowHeight;
	/*a = context->createContext();
	c = context->InitializeExtensions();*/
	b = context->create30Context();
	if(b)
	{
		inputMan = new WinInput();
		inputMan->Init(hInstance, hWnd, context->windowWidth, context->windowHeight);
	} else {
		MessageBox(NULL,
			(LPCWSTR)L"Could not initialize regular/3.0 rendering context or glew extensions",
			(LPCWSTR)L"Bye!",
			MB_ICONERROR| MB_OK | MB_DEFBUTTON2);
		exit(1);
	}
	context->setupScene();
	
	sceneMan->Init();
	sceneMan->ShadowMap = &shadowMap;

	if(!sceneMan->ImportAsset("models\\crate.ply"))
	{
		MessageBox(NULL,
			(LPCWSTR)L"Could not load model",
			(LPCWSTR)L"Bye!",
			MB_ICONERROR| MB_OK | MB_DEFBUTTON2);
		exit(1);
	}
	sceneMan->ElaborateScene();
	if(sceneMan->ImportAsset("models\\tipo_nuovo.ply"))
	{
		sceneMan->ElaborateScene2();
	}
	camera = new ThirdPersonCamera();
	camera->SetTimer(&timer);
	float index = sceneMan->getSIndex();
	float row = sceneMan->getSRow();
	qt = sceneMan->getQuadTree();
	camera->SetPosition(float3(2*index,0.f,2*row + 1));
	sceneMan->SetCamera(camera);
	context->manager = sceneMan;
	player = new flee::Object(2*index,2*row,1.0f,1.0f);
	POINT pt;
	pt.x = 0;
	pt.y = 0;
	ClientToScreen(hWnd, &pt); 
	SetCursorPos(pt.x,pt.y); 
	std::cout << "Initialize complete: took " << timer.Get_dt() << " msec"<< std::endl;

	// TEST: INIT MESH FROM LIST OF VERTEX
	hfbo.Init();
	hfbo.GenerateShader(std::string("shaders\\debug_drawtex.vs"),std::string("shaders\\debug_drawtex.ps"),std::string("shaders\\debug_drawtex.gs"));
	dtr = new DebugTextureRenderer();
	dtr->Init();
	dtr->GenerateShader(std::string("shaders\\debug_drawtex.vs"),std::string("shaders\\debug_drawtex.ps"),std::string("shaders\\debug_drawtex.gs"));
	BufferManager::get_instance()->FillBuffers();


	ShaderManager::get_instance()->SetShadowMVP(shadowMap.GetShadowMVP());
	newpos = (float3*)malloc(sizeof(float3));
	newpos->x = 0.0f;
	newpos->y = 0.0f;
	newpos->z = 0.0f;
}
void WinApplication::Deinit(void)
{
	shadowMap.Deinit();
	inputMan->cleanDInput();
	hfbo.~HUDFBO();
	delete(inputMan);
	delete(camera);
	MaterialManager::get_instance()->~MaterialManager();
	ShaderManager::get_instance()->~ShaderManager();
	BufferManager::get_instance()->~BufferManager();
	delete(sceneMan);
	//delete(timer);
	delete(context);
}

void WinApplication::Render(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	ShadowPass();
	RenderPass();
	HUDPass();
	//dtr->RenderTexture(shadowMap.GetShadowTexture());
	context->Swap();
}

void WinApplication::ShadowPass(void)
{
	shadowMap.Render();
}

void WinApplication::HUDPass()
{
	hfbo.SetView(glm::lookAt(glm::vec3(camera->GetPosition().x+11,camera->GetPosition().y-1,camera->GetPosition().z+11),glm::vec3(camera->GetPosition().x+11,camera->GetPosition().y,camera->GetPosition().z+11),glm::vec3(0,0,-1)));
	hfbo.Render(sceneMan->GetLabyrinth());
}

void WinApplication::RenderPass(void)
{
	glBindTexture(GL_TEXTURE_2D,shadowMap.GetShadowTexture());
	context->renderScene();
}
void WinApplication::Update(void)
{
	flee::input_state * in_state = inputMan->detect_input();
	timer.Update();
	float time = timer.Get_dt();
	float fps = 1000.0f/time;
	if(in_state->keyboard->state[DIK_O] & 0x80)
	{
		camera->SetType(FIRST_PERSON);
		tpc = false;
	}
	if(in_state->keyboard->state[DIK_P] & 0x80)
	{
		/// \todo check collisions with object, not camera
		camera->SetType(THIRD_PERSON);
		tpc = true;
	}
	float x = camera->GetPosition().x;
	float z = camera->GetPosition().z;
	float y = camera->GetPosition().y;
	/// \todo set correct x/y/z labels (something as "x: 1 - y: 5 - z:10")
	context->setWindowTitle(std::string("OpenGL 3 Project (FPS ").append(itoa_cpp((int)fps,10)).append(" - MS ").append(itoa_cpp((int)time,10).append(" ) ").append(itoa_cpp((int)x,10).append(itoa_cpp((int)y,10).append(itoa_cpp((int)z,10))))));
	sceneMan->Update();

	float index = sceneMan->getSIndex();
	float row = sceneMan->getSRow();
	//flee::float3 pastcampos(camera->GetPosition().x,camera->GetPosition().y,camera->GetPosition().z);

	in_map = true;
#if DEBUG
	if(camera->GetPosition().y >= 1 || camera->GetPosition().y <= -1)
		in_map = false;
#endif
	/*find objects near camera using quadtree*/
	vobj = qt->GetObjectsAt(camera->GetPosition().x, camera->GetPosition().z);

	if(in_state->keyboard->state[DIK_ESCAPE] & 0x80)
		running = false;
	camera->Update();
	if(in_state->keyboard->state[DIK_D] & 0x80)
	{
		if(!tpc)
		{
			if(!(CameraCollision(camera->FakeTranslate(flee::float3(-1,0,0)))))
				camera->Translate(flee::float3(-1,0,0));
		}else
			if(!(ModelCollision(camera->FakeTranslate(flee::float3(-1,0,0)))))
				camera->Translate(flee::float3(-1,0,0));
	}
	if(in_state->keyboard->state[DIK_A] & 0x80)
	{
		if(!tpc)
		{
			if(!(CameraCollision(camera->FakeTranslate(flee::float3(1,0,0)))))
				camera->Translate(flee::float3(1,0,0));
		}
		else
			if(!(ModelCollision(camera->FakeTranslate(flee::float3(1,0,0)))))
				camera->Translate(flee::float3(1,0,0));
	}
	if(in_state->keyboard->state[DIK_LSHIFT] & 0x80 && in_state->keyboard->state[DIK_W] & 0x80)
	{
		if(!tpc)
		{
			if(!(CameraCollision(camera->FakeTranslate(flee::float3(0,0,1.7)))))
				camera->Translate(flee::float3(0,0,1.7));
		}
		else
			if(!(ModelCollision(camera->FakeTranslate(flee::float3(0,0,1.7)))))
				camera->Translate(flee::float3(0,0,1.7));
	} 
	if(in_state->keyboard->state[DIK_W] & 0x80)
	{
		if(!tpc)
		{
			if(!(CameraCollision(camera->FakeTranslate(flee::float3(0,0,1)))))
				camera->Translate(flee::float3(0,0,1));
		}
		else
			if(!(ModelCollision(camera->FakeTranslate(flee::float3(0,0,1)))))
				camera->Translate(flee::float3(0,0,1));
	}
	if(in_state->keyboard->state[DIK_S] & 0x80)
	{
		if(!tpc)
		{
			if(!(CameraCollision(camera->FakeTranslate(flee::float3(0,0,-1)))))
				camera->Translate(flee::float3(0,0,-1));
		}
		else
			if(!(ModelCollision(camera->FakeTranslate(flee::float3(0,0,-1)))))
				camera->Translate(flee::float3(0,0,-1));
	}
#if DEBUG
	if(in_state->keyboard->state[DIK_SPACE] & 0x80)
	{
		camera->Translate(flee::float3(0,1,0));
	}
	if(in_state->keyboard->state[DIK_LCONTROL] & 0x80)
	{
		camera->Translate(flee::float3(0,-1,0));
	}
#endif
	if(in_state->keyboard->state[DIK_NUMPADENTER] & 0x80)
	{
		camera->following->SetPosition(float3(2*index,0.f,2*row + 1).toVec3());
		camera->SetPosition(float3(2*index,0.f,2*row + 1));
	}
	camera->LookAt(in_state->mouse->state->state.lX,in_state->mouse->state->state.lY);
	if(hasFocus){
		POINT pt;
		pt.x = context->windowWidth/2;
		pt.y = context->windowHeight/2;
		ClientToScreen(hWnd, &pt); 
		SetCursorPos(pt.x,pt.y); 
		ShowCursor(false);
	}
}

void WinApplication::SetHwnd(HWND h)
{
	this->hWnd = h;
}
void WinApplication::SetHinstance(HINSTANCE h)
{
	this->hInstance = h;
}
void WinApplication::Resize(int w, int h)
{
	context->windowWidth = w;
	context->windowHeight = h;
}

int WinApplication::CameraCollision(flee::float3 futcampos)
{
float3 camvel = futcampos.operator-(camera->GetPosition());
float delta_t = timer.Get_dt();
	if(!in_map)
		return (0);
	for(it = vobj.begin(); it != vobj.end(); it++)
	{
		if(Camera_Collide(((flee::Object*)*it), futcampos, camvel, delta_t))
			{
			//cout << "found collision!" << endl;
			return (1);
	}
		}
	return (0);
}

int WinApplication::Camera_Collide(flee::Object *object, flee::float3 futurepos, flee::float3 camvel, float dt) 
{
	float left, right, top, bottom;
	// bounding boxes
	left = object->x - object->width/2 - 1.0f;
	right = object->x + object->width/2 + 1.0f;
	top = object->y - object->height/2 - 1.0f;
	bottom = object->y + object->height/2 + 1.0f;
	float3 boxvect(0.f,0.f,0.f);

	//check collision
	
	if (futurepos.x < left){
		boxvect.x = 0.f; boxvect.y = camvel.y; boxvect.z = top - bottom;
		return(0);
		}

	if (futurepos.x > right) return(0);

	if (futurepos.z < top) return(0);
	
	if (futurepos.z > bottom) return(0);

	return(1);

};

int WinApplication::ModelCollision(flee::float3 futmodpos)
	{
	player->x = futmodpos.x;
	player->y = futmodpos.z;
	if(!in_map)
		return 0;
	for(it = vobj.begin(); it != vobj.end(); it++)
		{
		if(Model_Collide(((flee::Object*)*it), player))
			{
			//cout << "found model collision!" << endl;
			return (1);
			}
		}
	return (0);
	}

int WinApplication::Model_Collide(flee::Object *Wall, flee::Object *Model)
	{
	int M_left, W_left;
	int M_right, W_right;
	int M_top, W_top;
	int M_bottom, W_bottom;

	M_left = Model->x - 0.5;
	W_left = Wall->x - Wall->width/2;
	M_right = Model->x + 0.5;
	W_right = Wall->x + Wall->width/2;
	M_top = Model->y - 0.5;
	W_top = Wall->y - Wall->height/2;
	M_bottom = Model->y + 0.5;
	W_bottom = Wall->y + Wall->height/2;

	if (M_bottom < W_top) return(0);
	if (M_top > W_bottom) return(0);

	if (M_right < W_left) return(0);
	if (M_left > W_right) return(0);

	return(1);
	}
