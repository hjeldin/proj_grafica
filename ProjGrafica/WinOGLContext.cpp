﻿#include "StdAfx.h"
#include "WinOGLContext.h"
#include "SceneManager.h"
using namespace flee;
/**
	Default constructor for the OpenGLContext class. At this stage it does nothing
	but you can put anything you want here.
*/
WinOGLContext::WinOGLContext(void) {

}

/**
	Constructor for the OpenGLContext class which will create a context given a windows HWND.
*/
WinOGLContext::WinOGLContext(HWND hwnd) {
	this->hwnd = hwnd;
	create30Context(); // Create a context given a HWND
}

/**
	Destructor for our OpenGLContext class which will clean up our rendering context
	and release the device context from the current window.
*/
WinOGLContext::~WinOGLContext(void) {
    wglMakeCurrent(hdc, 0); // Remove the rendering context from our device context
    wglDeleteContext(hrc); // Delete our rendering context

    ReleaseDC(hwnd, hdc); // Release the device context from our window
}

/**
	create30Context creates an OpenGL context and attaches it to the window provided by
	the HWND. This method currently creates an OpenGL 3.2 context by default, but will default
	to an OpenGL 2.1 capable context if the OpenGL 3.2 context cannot be created.
*/
bool WinOGLContext::createContext() {
	hdc = GetDC(hwnd); // Get the device context for our window

	PIXELFORMATDESCRIPTOR pfd; // Create a new PIXELFORMATDESCRIPTOR (PFD)
	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR)); // Clear our  PFD
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR); // Set the size of the PFD to the size of the class
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW; // Enable double buffering, opengl support and drawing to a window
	pfd.iPixelType = PFD_TYPE_RGBA; // Set our application to use RGBA pixels
	pfd.cColorBits = 32; // Give us 32 bits of color information (the higher, the more colors)
	pfd.cDepthBits = 32; // Give us 32 bits of depth information (the higher, the more depth levels)
	pfd.iLayerType = PFD_MAIN_PLANE; // Set the layer of the PFD
	pfd.cAccumBits = 32;
	pfd.cAccumBlueBits = pfd.cAccumGreenBits = pfd.cAccumRedBits = pfd.cAccumAlphaBits = 8;
	int nPixelFormat = ChoosePixelFormat(hdc, &pfd); // Check if our PFD is valid and get a pixel format back
	if (nPixelFormat == 0) // If it fails
			return false;

	bool bResult = SetPixelFormat(hdc, nPixelFormat, &pfd); // Try and set the pixel format based on our PFD
	if (!bResult) // If it fails
		return false;

	dummyHrc = wglCreateContext(hdc); // Create an OpenGL 2.1 context for our device context
	wglMakeCurrent(hdc, dummyHrc); // Make the OpenGL 2.1 context current and active
	CheckErrors();
	return true;
}

bool WinOGLContext::InitializeExtensions(){
	glewExperimental = GL_TRUE;
	CheckErrors();
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
	  /* Problem: glewInit failed, something is seriously wrong. */
	 const GLubyte * b = glewGetErrorString(err);
	 std::cout << b;
	 return false;
	}
	CheckErrors();
	return true;
}


bool WinOGLContext::create30Context(){
	DummyWin32OGLContext    dummyCtx;

    dummyCtx.Open();
	CheckErrors();
    HDC hdc = GetDC( hwnd );
    this->hdc = hdc;

    int chooseAttribs[] = {
        WGL_SUPPORT_OPENGL_ARB,     GL_TRUE,
        WGL_PIXEL_TYPE_ARB,         WGL_TYPE_RGBA_ARB,          
        WGL_COLOR_BITS_ARB,         32,
        WGL_ALPHA_BITS_ARB,         8,
        WGL_DOUBLE_BUFFER_ARB,      GL_TRUE,
        WGL_ACCELERATION_ARB,       WGL_FULL_ACCELERATION_ARB,
        WGL_SAMPLE_BUFFERS_ARB,     GL_TRUE,
        WGL_SAMPLES_ARB,            16,
        0,0 };

    int     pixelFormat;
    UINT    numFormats;
    if ( FALSE == dummyCtx.mpWglChoosePixelFormatARB( hdc, chooseAttribs, NULL, 1, &pixelFormat, &numFormats ) )
    {
        DWORD err = GetLastError();
        std::cout <<  "Failed to choose the pixel format. Error" << err << std::endl;
		assert(0);
    }
	CheckErrors();
    static PIXELFORMATDESCRIPTOR    dummyPFD = {
        sizeof(PIXELFORMATDESCRIPTOR),  // Size of this structure
        1,                              // Version of this structure
        PFD_DRAW_TO_WINDOW |            // Draw to Window (not to bitmap)
        PFD_SUPPORT_OPENGL |            // Support OpenGL calls in window
        PFD_DOUBLEBUFFER |              // Double buffered mode
        PFD_STEREO_DONTCARE |
        0,
        PFD_TYPE_RGBA,                  // RGBA Color mode
        32,                     // Want the display bit depth
        0,0,0,0,0,0,                    // Not used to select mode
        0,0,                            // Not used to select mode
        0,0,0,0,0,                      // Not used to select mode
        32,                     // Size of depth buffer
        8,                   // bit stencil
        0,                              // Not used to select mode
        PFD_MAIN_PLANE,                 // Draw in main plane
        0,                              // Not used to select mode
        0,0,0
    };
    if ( FALSE == SetPixelFormat( hdc, pixelFormat, &dummyPFD ) )
    {
        DWORD err = GetLastError();
        std::cout <<  "Failed to choose the pixel format. Error" << err << std::endl;
		assert(0);
    }
	CheckErrors();
    int createAttrib[] = {
        WGL_CONTEXT_MAJOR_VERSION_ARB,  3,
        WGL_CONTEXT_MINOR_VERSION_ARB,  2,
        WGL_CONTEXT_FLAGS_ARB,          WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
        WGL_CONTEXT_PROFILE_MASK_ARB,   WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
		0,0
	};

	hrc = dummyCtx.mpWglCreateContextAttribsARB( hdc, dummyCtx.mHGLRC, createAttrib );
	CheckErrors();
    if ( hrc == NULL )
    {
        DWORD err = GetLastError();
        std::cout <<  "Failed to create ogl context. Error" << err << std::endl;
		assert(0);
    }
	const GLubyte * ptr = glGetString(GL_EXTENSIONS);
	CheckErrors();
    if(!wglMakeCurrent( hdc, hrc )){
		assert(0);
	}
	InitializeExtensions();
	CheckErrors();
    dummyCtx.Close();
    //wglMakeCurrent( pWin->mWIN32_HDC, pWin->mWIN32_HGLRC );
    if(!wglMakeCurrent( NULL, NULL ))
		assert(0);
    if(!wglMakeCurrent( hdc, hrc))
		assert(0);
	CheckErrors();
	return true;
}
/**
	setupScene will contain anything we need to setup before we render
*/
void WinOGLContext::setupScene(void) {
	glClearColor(0.f,0.f,0.f, 0.0f); // Set the clear color based on Microsofts CornflowerBlue (default in XNA)
	glEnable(GL_DEPTH_TEST);
	glEnable (GL_CULL_FACE);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glCullFace (GL_BACK);
	wglSwapIntervalEXT(0);			//disable vsync
}

/**
	reshapeWindow is called every time our window is resized, and it sets our windowWidth and windowHeight
	so that we can set our viewport size.
*/
void WinOGLContext::reshapeWindow(int w, int h) {
	windowWidth = w; // Set the window width
	windowHeight = h; // Set the window height
	manager->SetWindow(windowWidth, windowHeight);
}

/**
	renderScene will contain all our rendering code.

	The first thing we are going to do is set our viewport size to fill the entire window. 

	Next up we are going to clear our COLOR, DEPTH and STENCIL buffers to avoid overlapping
	of renders.

	Any of your other rendering code will go here.

	Finally we are going to swap buffers.
*/
void WinOGLContext::renderScene(void) {
    //glViewport(0, 0, windowWidth, windowHeight); // Set the viewport size to fill the window
	
	if(manager != NULL){
		manager->Render();
	}
	//CheckErrors();
	//Sleep(1);
}


void WinOGLContext::Swap(void)
{
	SwapBuffers(hdc); // Swap buffers so we can see our rendering
}

void WinOGLContext::SetHandle(HWND handle)
{
	this->hwnd = handle;
}

void WinOGLContext::CheckErrors(void ){
#if DEBUG
	GLenum a = glGetError();
	if(a != GL_NO_ERROR){
		std::cout << glewGetErrorString(a) << "from winoglcontext.h"<< std::endl;
	}
#endif
}
void WinOGLContext::setWindowTitle(std::string str)
{
	const char *orig = str.c_str(); // Our windows title
	size_t origsize = strlen(orig) + 1;
    const size_t newsize = 100;
    size_t convertedChars = 0;
    wchar_t wcstring[newsize];
    mbstowcs_s(&convertedChars, wcstring, origsize, orig, _TRUNCATE);
	SetWindowText(hwnd,wcstring);
}