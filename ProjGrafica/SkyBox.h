#pragma once
#include <direct.h> 
#include "IL\il.h"
#include "Mesh.h"
#include "ShaderManager.h" 
namespace flee{
	/**
		Skybox provides functionality to create a cubemap (6 textures) and display it on screen.
	*/
	class SkyBox
	{
	public:
		/**
			Default constructor
		*/
		SkyBox(void);
		/**
			Default destructor
		*/
		~SkyBox(void);
		/**
			Loads 6 textures to create a cubemap
			\param xp name of the positive x texture
			\param xn name of the negative x texture
			\param yp name of the positive y texture
			\param yn name of the negative y texture
			\param zp name of the positive z texture
			\param zn name of the negative z texture
			\return true if creation was successful
		*/
		bool Load(std::string xp,std::string xn, std::string yp, std::string yn, std::string zp, std::string zn);
		/**
			Binds the cubemap to the sampler
			\param TextureUnit GL_TEXTUREx where x is the sampler number
		*/
		void Bind(GLenum TextureUnit);
		/**
			Renders the mesh with associated texture on screen
		*/
		void Render();

		/**
			Setter for projection matrix
			\param proj projection matrix
		*/
		void SetProjection(glm::mat4 proj);
		/**
			Setter for view matrix
			\param view view matrix
		*/
		void SetView(glm::mat4 view);
		/**
			Setter for model matrix
			\param model model matrix
		*/
		void SetModel(glm::vec3 model);
		/**
			Converts file to std::string
			\param filename path to file
			\return the string
		*/
		std::string file2string(std::string filename );
		/**
			Uses the skybox shader program (skybox.vs/skybox.ps)
		*/
		void SetActive();
		/**
			Pointer to mesh (usually a cube) that should be drawn
		*/
		Mesh* m_pMesh;
	private:
		GLuint mTextureObj;
		ILuint mILObj;

		GLuint shaderProgram,mFs,mVs;
		GLuint modelMatrix,viewMatrix,projectionMatrix;
		glm::mat4x4 modelM,viewM,projM;
		std::string shaders[2];
	};
};