#pragma once
#include <vector>
#include "defs.h"

namespace flee{
	class Light{
	public:
		Light(void);
		Light(glm::vec3 position, glm::vec3 color);
		~Light(void);
		const glm::vec3 * GetColor();
		const glm::vec3 * GetPosition();
	protected:
		glm::vec3 position;		
		glm::vec3 color;
	};

	class PointLight:public Light
	{
	public:
		PointLight(void);
		PointLight(glm::vec3 position, glm::vec3 color);
		~PointLight(void);
	};

	class SpotLight:public Light
	{
	public:
		SpotLight(glm::vec3 position, glm::vec3 direction, glm::vec3 color);
		~SpotLight(void);
		const glm::vec3 * GetDirection(void);
	private:
		glm::vec3 direction;
	};

	
	class LightManager 
	{
	public:
		LightManager(void);
		~LightManager(void);
		void CreateLight(glm::vec3 position, glm::vec3 direction, glm::vec3 color);
		const std::vector<Light * > & GetLights();
	private:
		std::vector<Light *> lightList;
	};
};