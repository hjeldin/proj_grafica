#include "StdAfx.h"
#include "Model.h"

using namespace flee;

Model::Model(void)
{
	this->isLoaded = false;
	this->scale = glm::vec3(1,1,1);
}

Model::Model(std::string name)
{
	this->name = name;
	this->scale = glm::vec3(1,1,1);

}

Model::Model(std::string name, aiMesh * mesh, aiMaterial * material)
{
	this->name = name;
	this->mesh = new Mesh(mesh);
	this->mat = new Material(material);
	this->scale = glm::vec3(1,1,1);

}

Model::Model(std::string name, aiMesh * mesh, Material * material)
{
	this->name = name;
	this->mesh = new Mesh(mesh);
	this->mat = material;
	this->scale = glm::vec3(1,1,1);
}

Model::Model(std::string name, Mesh * mesh, Material * material)
{
	this->name = name;
	this->mesh = mesh;
	this->mat = material;
	this->scale = glm::vec3(1,1,1);
}

Model::~Model(void)
{
	this->Shutdown();
}

GLuint Model::Initialize(void)
{
	//load mesh, apply material
	//mesh->Initialize();
	rotationX = rotationY = rotationZ = 0;
	shaderMan = ShaderManager::get_instance();
	mat->Initialize();
	shaderProgram = mat->GetShaderProgram();

	isLoaded = true;
	bbox = AABox(Vec3(1.0f,1.f,1.f),position.x,-1,position.z);
	return mat->GetShaderProgram();
}

void Model::Shutdown(void)
{
	isLoaded = false;
	this->mesh->Shutdown();
	this->mat->Shutdown();
	if(mesh!=0)
	{
		delete this->mesh;
	}
	if(mat!=0)
	{
		delete this->mat;
	}
}


void Model::Draw(void)
{
	if(!isLoaded) return;

	if(!mat->IsEnabled()) return;	
	mat->UseProgram();
	//should be decommented when needed
	//glBindVertexArray(mesh->GetVAO());
	//if(visible)
		this->mesh->Draw();
}

void Model::Translate(glm::vec3 t, float msec,float rX, float rY, float rZ)
{
	rotationX = rX;
	rotationY = rY;
	rotationZ = rZ;
	if(t.x && t.y && t.z)
	{
		std::cout << " lol " << std::endl;
	}
	if(t.z!= 0.0f && t.x != 0.0f) {
		position.x += 5.f * sin(rotationX) * msec * t.z * t.x;
		position.z += 5.f * cos(rotationX) * msec * t.z * t.x;	
	} else {
		if( t.x != 0.0f)
		{
			position.x += 5.f * cos(rotationX) * msec * t.x;
			position.z -= 5.f * sin(rotationX) * msec * t.x;	
		}
		if(t.z != 0.0f)
		{
			position.x += 5.f * sin(rotationX) * msec * t.z;
			position.z += 5.f * cos(rotationX) * msec * t.z;
		}
	}
	if(t.y != -0.f){
		position.y += 5.f * msec * t.y;
	}
	RecalcModelMatrix();
}

void Model::Rotate(float x, float y, float z)
{
	rotationX = x;
	rotationY = y;
	rotationZ = z;
	RecalcModelMatrix();
}