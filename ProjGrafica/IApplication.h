#pragma once

namespace flee{
	class IApplication
	{
	public:
		virtual void Init(void) = 0;
		virtual void Deinit(void) = 0;
		virtual void Render(void) = 0;
		virtual void Update(void) = 0;
	};
}
