//#ifdef _MSC_VER

#include "stdafx.h"
#include "ProjGrafica.h"
#include "defs.h"
#include "WinApplication.h" 
//#pragma comment(linker, "/entry:WinMain")
#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"assimp.lib")
#pragma comment(lib,"DevIL.lib")
#pragma comment(lib,"ilu.lib")

flee::WinApplication app;
int flee::windowHeight = 768;
int flee::windowWidth = 1024;

/**
	WndProc is a standard method used in Win32 programming for handling Window messages. Here we
	handle our window resizing and tell our OpenGLContext the new window size.
*/
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		
		case WM_CREATE:
			printf("Created\n");
			break;
		case WM_ACTIVATE:	

			if (LOWORD(wParam) == WA_INACTIVE)		
				app.hasFocus = false;	
			else		
				app.hasFocus = true;
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		case WM_QUIT:
			running = false;
			break;
		case WM_SIZE: // If our window is resizing
		{
			//openglContext.reshapeWindow(LOWORD(lParam), HIWORD(lParam)); // Send the new window size to our OpenGLContext
			//app.Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		}

	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

/**
	createWindow is going to create our window using Windows API calls. It is then going to
	create our OpenGL context on the window and then show our window, making it visible.
*/
HWND createWindow(LPCWSTR title, int width, int height) {
	
	WNDCLASS windowClass;
	HWND hWnd;
	DWORD dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;

	hInstance = GetModuleHandle(NULL);

	windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	windowClass.lpfnWndProc = (WNDPROC) WndProc;
	windowClass.cbClsExtra = 0;
	windowClass.cbWndExtra = 0;
	windowClass.hInstance = hInstance;
	windowClass.hIcon = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON1));
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowClass.hbrBackground = NULL;
	windowClass.lpszMenuName = NULL;
	windowClass.lpszClassName = title;

	if (!RegisterClass(&windowClass)) {
		return false;
	}

	//Create window and set hWnd to it's handle
	hWnd = CreateWindowEx(dwExStyle, title, title, WS_CAPTION | WS_OVERLAPPED | WS_SYSMENU ,
      CW_USEDEFAULT, 0, width, height, NULL, NULL, hInstance, NULL);

	ShowWindow(hWnd, SW_SHOW);
    UpdateWindow(hWnd);

	return hWnd;
}

/**
	WinMain is the main entry point for Windows based applications as opposed to 'main' for console
	applications. Here we will make the calls to create our window, setup our scene and then
	perform our 'infinite' loop which processes messages and renders.
*/
int WINAPI WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR    lpCmdLine,
                     int       nCmdShow) {

	MSG msg;
	//Create console and attach stdout
	int fd;
	FILE *fp;
	HANDLE hCon;

	AllocConsole();

	hCon = GetStdHandle(STD_OUTPUT_HANDLE);

	fd = _open_osfhandle(reinterpret_cast<long>(hCon), 0);
	fp = _fdopen(fd, "w");

	*stdout = *fp;
	setvbuf(stdout, NULL, _IONBF, 0);
	/**
		The following 6 lines of code do conversion between char arrays and LPCWSTR variables
		which are used in the Windows API.
	*/
	char *orig = "OpenGL 3 Project"; // Our windows title
	size_t origsize = strlen(orig) + 1;
    const size_t newsize = 100;
    size_t convertedChars = 0;
    wchar_t wcstring[newsize];
    mbstowcs_s(&convertedChars, wcstring, origsize, orig, _TRUNCATE);

	//CREATE WINDOWS GAME APPLICATION	
	HWND hWnd = createWindow(wcstring, flee::windowWidth,flee::windowHeight);
	SetWindowPos(hWnd, HWND_TOP, 0,20,flee::windowWidth,flee::windowHeight,NULL);
	app.SetHwnd(hWnd);
	app.SetHinstance(hInstance);
	app.Init();

	bool hasFocus = true;
	/**
		This is our main loop, it continues for as long as running is true
	*/
	while (app.running)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) { // If we have a message to process, process it
				TranslateMessage(&msg);
				DispatchMessage(&msg);
		}
		else {	
			app.Update();
			app.Render();
		}
	}

	app.Deinit();
	//XXX: should free all allocated memory from openglContext and exit without memory leaks (see gdebugger's problems)

	return (int) msg.wParam;
}
