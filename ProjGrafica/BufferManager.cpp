#include "StdAfx.h"
#include "BufferManager.h"

using namespace flee;

BufferManager * BufferManager::instance_ptr = NULL;

BufferManager::BufferManager(void)
{
	vertices = new std::vector<vertex>();
	indices = new std::vector<int>();
	VBOs = new std::vector<GLuint>();
	VAOs = new std::vector<GLuint>();
	occupiedVBOS = new std::vector<int>();
	offsets = 0;
	vOffset = 0;
	GenerateNewVAO();
}

unsigned int BufferManager::Add(Mesh * mesh)
{
	vOffset = offsets = 0;
	meshes.push_back(mesh);
	std::vector<Mesh *>::iterator mIt;
	GLuint cVAO;
	std::vector<vertex>::iterator itV;
	std::vector<int>::iterator itI;
	offsets = 0;
	for(mIt = meshes.begin(); mIt<meshes.end();mIt++)
	{
		if(*mIt != mesh){
			printf("%d\n",(*mIt)->GetVertices()->size());
			vOffset += (*mIt)->GetVertices()->size();
			offsets += (*mIt)->GetIndices()->size() * sizeof(int);
		} else {
			mesh->SetIOffset(offsets);
			mesh->SetVOffset(vOffset);
		}
		//mVertices->push_back(
	}
	
	itV = vertices->end();
	itI = indices->end();
	vertices->insert(itV,mesh->GetVertices()->begin(), mesh->GetVertices()->end());
	indices->insert(itI,mesh->GetIndices()->begin(), mesh->GetIndices()->end());
	return VAOs->at(0);
	
}

void BufferManager::FillBuffers()
{
	std::vector<vertex>::iterator itV;
	std::vector<int>::iterator itI;
	vao_str * vertices_str = new vao_str[vertices->size()];
	unsigned long int i=0;
	for ( itV=vertices->begin() ; itV < vertices->end(); itV++ )
	{
		vertices_str[i].x = itV->position.x;
		vertices_str[i].y = itV->position.y;
		vertices_str[i].z = itV->position.z;
		vertices_str[i].r = itV->color.x;
		vertices_str[i].g = itV->color.y;
		vertices_str[i].b = itV->color.z;
		vertices_str[i].a = itV->color.w;
		vertices_str[i].nx = itV->normal.x;
		vertices_str[i].ny = itV->normal.y;
		vertices_str[i].nz = itV->normal.z;
		if(vertices_str[i].nx == 0 && vertices_str[i].ny == 0 && vertices_str[i].nz == 0)
			std::cout << "porcodddddddio" <<std::endl;
		vertices_str[i].ts1 = itV->t;
		vertices_str[i].ts2 = itV->v;
		vertices_str[i].tr1 = itV->s;
		vertices_str[i].tr2 = 0;
		vertices_str[i].tq1 = 0;
		vertices_str[i].tq2 = 0;
		i++;
	}
	glBindVertexArray(VAOs->at(0));
	//now i can bind the current buffer and push vertices in it
	glBindBuffer(GL_ARRAY_BUFFER,VBOs->at(VBOs->size()-2));
	glBufferData(GL_ARRAY_BUFFER, sizeof(vao_str) * vertices->size(), vertices_str, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vao_str), (void*)offsetof(vao_str, x));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(vao_str), (void*)offsetof(vao_str, r));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(vao_str), (void*)offsetof(vao_str, nx));
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(vao_str), (void*)offsetof(vao_str, ts1));
	i = 0;

	int * indices_str = new int[indices->size()];
	for ( itI=indices->begin() ; itI < indices->end(); itI++ )
	{
		indices_str[i] = *itI;
		//std::cout << "index in position " << i << ":" << *itI << std::endl;
		i++;
	}
	//bind index buffer and push indices in it
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,VBOs->at(VBOs->size()-1));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(int) * i, indices_str, GL_STATIC_DRAW);
	

	delete [] vertices_str;
	delete [] indices_str;
	return;
}

void BufferManager::Remove(Mesh * pMesh)
{
	GLuint vao = pMesh->GetVAO();
	//check if it's the last mesh of the vao, if so, delete it
	//pMesh.reset();
	glDeleteBuffers(2,newVbo);
	glDeleteVertexArrays(1,&vao);
	VAOs->clear();
	VBOs->clear();
	occupiedVBOS->clear();
	meshes.clear();
	GenerateNewVAO();
	vOffset = 0;
	offsets = 0;
	vertices->clear();
	indices->clear();
}

unsigned int BufferManager::GenerateNewVAO(void)
{
	//create vertex array object
	glGenVertexArrays(1,&newVao);
	VAOs->push_back(newVao);
	//bind vertex array object and create 2 vbo
	glBindVertexArray(newVao);
	glGenBuffers(2, newVbo);
	//set the first VBO to hold vertex data
	glBindBuffer(GL_ARRAY_BUFFER, newVbo[0]);
	//set the second VBO to hold index data
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, newVbo[1]);
	VBOs->push_back(newVbo[0]);
	VBOs->push_back(newVbo[1]);
	//set occupied space for new buffers to zero
	occupiedVBOS->push_back(0);

	return newVao;
}

BufferManager::~BufferManager(void)
{
	int i=0;
	std::vector<GLuint>::iterator it;
	GLuint * VBOSToDelete = new GLuint[VBOs->size()];
	for ( it=VBOs->begin() ; it < VBOs->end(); it++ )
	{
		VBOSToDelete[i] = *it;
		i++;
	}
	glDeleteBuffers(VBOs->size(),VBOSToDelete);
	i=0;
	GLuint * VAOSToDelete = new GLuint[VAOs->size()];
	for ( it=VAOs->begin() ; it < VAOs->end(); it++ )
	{
		VAOSToDelete[i] = *it;
		i++;
	}
	glDeleteVertexArrays(VAOs->size(),VAOSToDelete);
	glBindVertexArray(0);
}

void BufferManager::CheckError(void)
{
#if DEBUG
	GLenum a = glGetError();
	if(a != GL_NO_ERROR)
	{
		std::cout<< "Error while generating buffers" << std::endl;
	}
#endif
}