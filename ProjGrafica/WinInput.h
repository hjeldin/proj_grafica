#pragma once
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>


#ifdef _MSC_VER
#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")
namespace flee{

	typedef struct tagMouse
	{
		int x, y,
		pinstate,
		pin;
		DIMOUSESTATE state;
	} mouse_t;

	typedef struct keyState{
		BYTE * state;
	} k_state;

	typedef struct moState{
		mouse_t * state;
	} m_state;

	typedef struct i_state{
		k_state * keyboard;
		m_state * mouse;
	} input_state;

	class WinInput
	{
	public:
		WinInput(void);
		~WinInput(void);
		void Init(HINSTANCE hInstance, HWND hWnd, int width, int height);    // sets up and initializes DirectInput
		input_state * detect_input(void);		// gets the current input state
		void cleanDInput(void);					// closes DirectInput and releases memory

	private:
		LPDIRECTINPUT8 din;						// the pointer to our DirectInput interface
		LPDIRECTINPUTDEVICE8 dinkeyboard;		// the pointer to the keyboard device
		BYTE keystate[256];						// the storage for the key-information
		LPDIRECTINPUTDEVICE8 dinmouse;			// the pointer to the mouse device
		DIMOUSESTATE mousestate;				// the storage for the mouse-information
		HWND hWnd;								// window handle pointer
		int max_x, max_y;						// screen bounds
		mouse_t mse;							// mouse state 
		input_state current_state;				// current state (mouse+keyboard)
		input_state previous_state;				// previous state(mouse + keyboard)
	};

}

#endif