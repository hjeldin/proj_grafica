#pragma once
#include "Mesh.h"
#include <string>
#include <map>

namespace flee{
	/**
		Singleton mesh manager.

		Keeps track of every allocated mesh.
	*/
	class MeshManager
	{
	public:
		/**
			Default destructor
		*/
		~MeshManager(void);

		/**
			Singleton access to mesh manager
		*/
		static MeshManager* get_instance()
		{
			if(instance_ptr == NULL){
				instance_ptr = new MeshManager();
			}
			return instance_ptr;
		}
		/**
			Adds a mesh into mesh map.
			\param pointer to mesh
		*/
		void Add(Mesh *);
		/**
			Removes a mesh from mesh map.
			\param pointer to mesh
		*/
		void Remove(Mesh *);
		/**
			Erase mesh map
		*/
		void Purge();

		/**
			Getter for specified key
			\param str to mesh
			\return pointer to mesh
		*/
		Mesh * Get(std::string &str);
	private:
		MeshManager(void);	/// Private constructor
		std::map<std::string, Mesh *> meshMap;	///Mesh map, specified by key
		static MeshManager * instance_ptr;	/// Instance pointer to singleton
	};
}