#include "StdAfx.h"
#include "ShaderManager.h"

using namespace flee;
ShaderManager * ShaderManager::instance_ptr = NULL;
ShaderManager::ShaderManager(void)
{
}


ShaderManager::~ShaderManager(void)
{
	std::vector<GLuint>::iterator it;
	GLenum e;
	for(it = shaderPrograms.begin();it != shaderPrograms.end(); it++)
	{
		GLuint progr = *it;
		//glDeleteProgram(*it);
	}
}

GLuint ShaderManager::CompileShaderOfType(GLenum type, std::string & shader)
{
	GLuint pshader = glCreateShader(type);
	const GLchar * point = shader.c_str();
	glShaderSource(pshader, 1, &point, 0);
	glCompileShader(pshader);
	GLint IsCompiled_VS;
	glGetShaderiv(pshader, GL_COMPILE_STATUS, &IsCompiled_VS);
	if(IsCompiled_VS == FALSE)
	{
		int maxLength;
		glGetShaderiv(pshader, GL_INFO_LOG_LENGTH, &maxLength);
		char * vertexInfoLog =new char[maxLength];
		glGetShaderInfoLog(pshader, maxLength, &maxLength, vertexInfoLog);
		std::cout << "ERROR COMPILING SHADER:"<< vertexInfoLog << shader << std::endl;
		delete []vertexInfoLog;
		return -1;
	}
	return pshader;
}

GLuint ShaderManager::LinkShaders(GLuint mVertexshader, GLuint mFragmentshader, GLuint mGeometryshader)
{
	GLuint mShaderProgram = glCreateProgram();
	glAttachShader(mShaderProgram, mVertexshader);
	GLenum a = glGetError();
	glAttachShader(mShaderProgram, mFragmentshader);
	a = glGetError();
	if(mGeometryshader != 0){
		glAttachShader(mShaderProgram, mGeometryshader);
		a = glGetError();
	}
	glLinkProgram(mShaderProgram);

	a = glGetError();
	GLint IsLinked;
	glGetProgramiv(mShaderProgram, GL_LINK_STATUS, (int *)&IsLinked);
	if(IsLinked == FALSE)
	{
		int maxLength;
		glGetProgramiv(mShaderProgram, GL_INFO_LOG_LENGTH, &maxLength);
		char * shaderProgramInfoLog = new char[maxLength];
		glGetProgramInfoLog(mShaderProgram, maxLength, &maxLength, shaderProgramInfoLog);
		delete[]shaderProgramInfoLog;
		return -1;
	}
	glBindFragDataLocation(mShaderProgram,0,"fragColor");
	a = glGetError();
	std::cout << a << std::endl;	
	return AddProgram(mShaderProgram);
}

int ShaderManager::AddProgram(int program)
{
	shaderPrograms.push_back(program);
	return shaderPrograms.size()-1;
}

GLuint ShaderManager::GetProgram(int program)
{
	return shaderPrograms[program];
}

void ShaderManager::SetLightsUniform(const std::vector<Light *> * lights)
{
	int n=0;
	std::vector<Light *>::const_iterator itL;
	float positionsPtr[20*3];
	float directionsPtr[20*3];
	float colorsPtr[20*3];
	for(itL = lights->begin();itL!=lights->end();itL++)
	{
		positionsPtr[n*3+0] = (*itL)->GetPosition()->x;
		positionsPtr[n*3+1] = (*itL)->GetPosition()->y;
		positionsPtr[n*3+2] = (*itL)->GetPosition()->z;
		colorsPtr[n*3+0] = (*itL)->GetColor()->x;
		colorsPtr[n*3+1] = (*itL)->GetColor()->y;
		colorsPtr[n*3+2] = (*itL)->GetColor()->z;
		//directionsPtr[n*3+1] = *(*itL)->GetDirection();
	}

	std::vector<GLuint>::iterator it;
	GLenum e;
	n=0;
	for(it = shaderPrograms.begin();it != shaderPrograms.end(); it++)
	{
		glUseProgram(shaderPrograms[n]);
		int a = glGetUniformLocation(shaderPrograms[n],"u_lightPos");
		if(a != -1)
			glUniform3fv(a,20,positionsPtr);
		/*int a = glGetUniformLocation(shaderPrograms[n],"u_lightPos");
		glUniform3fv(a,20,positionsPtr);*/
		a = glGetUniformLocation(shaderPrograms[n],"u_lightDiffCol");
		if(a!=-1)
			glUniform3fv(a,20,colorsPtr);
		n++;
	}
	
}

void ShaderManager::GetUniforms(int program)
{
	modelMatrixLocation.push_back(glGetUniformLocation(shaderPrograms[program], "u_model"));
	viewMatrixLocation.push_back(glGetUniformLocation(shaderPrograms[program], "u_view"));
	projectionMatrixLocation.push_back(glGetUniformLocation(shaderPrograms[program], "u_projection"));
}
void ShaderManager::SetUniforms(int program, glm::mat4 model, glm::mat4 view, glm::mat4 projection)
{
	glUseProgram(shaderPrograms[program]);
	glUniformMatrix4fv(projectionMatrixLocation[program] , 1, GL_FALSE, glm::value_ptr(projection));
	glUniformMatrix4fv(modelMatrixLocation[program] , 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(viewMatrixLocation[program] , 1, GL_FALSE, glm::value_ptr(view));
}

void ShaderManager::SetUniforms(int program, glm::mat4 model, glm::mat4 view)
{
	glUseProgram(shaderPrograms[program]);
	glUniformMatrix4fv(modelMatrixLocation[program] , 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(viewMatrixLocation[program] , 1, GL_FALSE, glm::value_ptr(view));
}

void ShaderManager::SetUniforms(int program, glm::mat4 model)
{
	glUseProgram(shaderPrograms[program]);
	int a = glGetUniformLocation(shaderPrograms[program],"u_model");
	glUniformMatrix4fv(a, 1, GL_FALSE, glm::value_ptr(model));
}

void ShaderManager::SetShadowMVP(glm::mat4 mat)
{
	std::vector<GLuint>::iterator it;
	GLenum e;
	int n=0;
	for(it = viewMatrixLocation.begin();it != viewMatrixLocation.end(); it++)
	{
		glUseProgram(shaderPrograms[n]);
		int a = glGetUniformLocation(shaderPrograms[n],"u_shadowMVP");
		glUniformMatrix4fv(a,1,GL_FALSE,glm::value_ptr(mat));
		n++;
	}
}

void ShaderManager::SetUniforms(glm::mat4 view)
{
	std::vector<GLuint>::iterator it;
	GLenum e;
	int n=0;
	for(it = shaderPrograms.begin();it != shaderPrograms.end(); it++)
	{
		glUseProgram(shaderPrograms[n]);
		int a = glGetUniformLocation(shaderPrograms[n],"u_view");
		glUniformMatrix4fv(a,1,GL_FALSE,glm::value_ptr(view));
		/*if((e=glGetError()) != 0)
		{
		std::cout << "ERRORE: " << e << std::endl;
		}*/
		n++;
	}
}


void ShaderManager::SetProjection(glm::mat4 proj)
{
	std::vector<GLuint>::iterator it;
	for(it = projectionMatrixLocation.begin();it != projectionMatrixLocation.end(); it++)
	{
		glUniformMatrix4fv(*it,1,GL_FALSE,glm::value_ptr(proj));
	}
}

void ShaderManager::UseProgram(int program)
{
	glUseProgram(shaderPrograms[program]);
}

void ShaderManager::SetPlayerPos(glm::vec3 position)
{
	std::vector<GLuint>::iterator it;
	int n=0;
	for(it = shaderPrograms.begin();it != shaderPrograms.end(); it++)
	{
		glUseProgram(shaderPrograms[n]);
		GLuint a = glGetUniformLocation(shaderPrograms[n],"u_playerPos");
		if(a != (GLuint)-1)
			glUniform3f(a,position.x,position.y,position.z);

		n++;
	}
}