#include "stdafx.h"
#include "LightManager.h"

using namespace flee;
Light::Light(void)
{
	position = glm::vec3(0,1,0);
	color = glm::vec3(1,1,1);
}

Light::~Light(void)
{

}

const glm::vec3 * Light::GetColor(void)
{
	return &color;
}

const glm::vec3 * Light::GetPosition(void)
{
	return &position;
}

PointLight::PointLight()
{
	Light::Light();
}
PointLight::PointLight(glm::vec3 position, glm::vec3 color)
{
	this->position = position;
	this->color = color;
}
PointLight::~PointLight()
{
	Light::~Light();
}

SpotLight::SpotLight(glm::vec3 position, glm::vec3 direction, glm::vec3 color)
{
	this->position = position;
	this->color = color;
	this->direction = direction;
}

SpotLight::~SpotLight(void)
{
	Light::~Light();
}

const glm::vec3 * SpotLight::GetDirection(void)
{
	return &direction;
}

LightManager::LightManager(void)
{
}


LightManager::~LightManager(void)
{
}

void LightManager::CreateLight(glm::vec3 position, glm::vec3 direction, glm::vec3 color)
{
	Light * a;
	if(direction != glm::vec3(0,0,0)){
		a = new SpotLight(position,direction,color);
	} else {
		a = new PointLight(position,color);	
	}
	lightList.push_back(a);
}

const std::vector<Light*> & LightManager::GetLights(void)
{
	return (this->lightList);
}