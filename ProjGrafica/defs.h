#pragma once
#if _MSC_VER
//#include "WinInput.h"
#endif
#include <glm\glm.hpp>
#include <iostream>
#include <fstream>
namespace flee{
	
	extern int windowWidth;
	extern int windowHeight;

	enum CameraType {
		FIRST_PERSON,	///SELF EXPLAINING
		THIRD_PERSON	///SELF EXPLAINING
	};
	struct float4
	{
		float x;
		float y;
		float z;
		float w;
		float4(float _x, float _y, float _z, float _w):x(_x),y(_y),z(_z),w(_w) {};
		float4(){};

		inline float4 operator-(const float4 &a)
		{
			return float4(this->x - a.x,this->y - a.y,this->z - a.z,this->w);
		}
		inline float4 operator+(const float4 &a)
		{
			return float4(this->x + a.x,this->y + a.y,this->z + a.z,this->w);
		}
		inline float4 operator*(const float4 &a)
		{
			return float4(this->x*a.x,this->y*a.y,this->z*a.z,this->w*a.w);
		}
		inline float4 operator*(const float &a)
		{
			return float4(this->x*a,this->y*a,this->z*a,this->w*a);
		}
		inline float4 operator/(const float &a)
		{
			return float4(this->x/a,this->y/a,this->z/a,this->w);
		}

		inline float4 cross(const float4 &B)
		{
			float4 vector;
			vector.x = this->y*B.z - B.y*this->z;
			vector.y = B.x*this->z - this->x*B.z;
			vector.z = this->x*B.y - this->y*B.x;
			//TODO: check if it is right
			vector.w = (this->w == B.w)?this->w:0;
			return vector;
		}
		inline float dot(float4 &a,float4 &b)
		{
			float dp = 0.0f;
			dp += a.x * b.x;
			dp += a.y * b.y;
			dp += a.z * b.z;
			return dp;
		}
		inline float magnitude()
		{
			return sqrt((this->x*this->x + this->y*this->y + this->z * this->z));
		}
		inline float4 normalize()
		{
			float mag = this->magnitude();
			return float4(this->x / mag, this->y / mag, this->z / mag, this->w);
		}
	};

	struct float3
	{
		float x;
		float y;
		float z;
		float3(float _x, float _y, float _z):x(_x),y(_y),z(_z) {};
		float3():x(0),y(0),z(0){};

		inline glm::vec3 toVec3(){
			return glm::vec3(x,y,z);
		}
		inline float3 operator-(const float3 &a)
		{
			return float3(this->x - a.x,this->y - a.y,this->z - a.z);
		}
		inline float3 operator-(void)
		{
			return float3(-x,-y,-z);
		}
		inline float3 operator+(const float3 &a)
		{
			return float3(this->x + a.x,this->y + a.y,this->z + a.z);
		}
		inline float3 operator*(const float3 &v) {

			float3 res;

			res.x = y * v.z - z * v.y;
			res.y = z * v.x - x * v.z;
			res.z = x * v.y - y * v.x;

			return (res);
		}
		inline float3 operator*(const float &a)
		{
			return float3(this->x*a,this->y*a,this->z*a);
		}
		inline float3 operator/(const float &a)
		{
			return float3(this->x/a,this->y/a,this->z/a);
		}
		inline float3 cross(const float3 &B)
		{
			float3 vector;
			vector.x = this->y*B.z - B.y*this->z;
			vector.y = B.x*this->z - this->x*B.z;
			vector.z = this->x*B.y - this->y*B.x;
			return vector;
		}
		inline float innerProduct(float3 &b)
		{
			float dp = 0.0f;
			dp += this->x * b.x;
			dp += this->y * b.y;
			dp += this->z * b.z;
			return dp;
		}
		inline void copy(const float3 &v)
		{
			x = v.x; y=v.y; z=v.z;
		}
		inline float3 scalarMult(float a)
		{
			return float3(x*a, y*a, z*a);
		}
		inline void print()
		{
			printf("vec3(%f %f %f)",x,y,z);
		}

		inline void set(float x, float y, float z)
		{
			this->x = x;
			this->y = y;
			this->z = z;
		}
		inline float length()
		{
			return sqrt(((this->x*this->x) + (this->y*this->y) + (this->z * this->z)));
		}

		inline void normalize()
		{
			float mag = this->length();
			if(mag)
			{ 
				x /= mag;
				y /= mag;
				z /= mag;
			}
		}
	};

	struct vertex
	{
		float4 position;
		float4 color;
		float3 normal;
		float t,v,s;
		vertex(float4 _position, float3 _normal, float _t, float _v):position(_position), color(float4(1.0f,1.0f,1.0f,1.0f)),normal(_normal),t(_t),v(_v){};
		vertex(float4 _position, float3 _normal, float _t, float _v,float _s):position(_position), color(float4(1.0f,1.0f,1.0f,1.0f)),normal(_normal),t(_t),v(_v),s(_s){};
		vertex(float4 _position, float4 _color, float3 _normal): position(_position),color(_color),normal(_normal) {};
		vertex(float4 _position): position(_position), color(float4(1.0f,1.0f,1.0f,1.0f)) 
		{
		};
		vertex(){};
		void CalculateNormal(vertex a, vertex b)
		{
			float4 dir = (a.position-this->position).cross((b.position-this->position));
			float4 k = (dir / dir.magnitude());
			this->normal = float3(k.x,k.y,k.z);	
			a.normal = this->normal;
			b.normal = this->normal;
			//XXX:NOPE!
		}
	};

	inline void CheckOGLError()
	{
#if DEBUG
		GLenum a = glGetError();
		if(a != GL_NO_ERROR)
		{
			std::cout<< "Error!" << std::endl;
		}
#endif

	}

	/**
		Transforms a map to a vector.
		\param m pointer to map that should be copied
		\param v pointer to vector that should contain the copy
	*/
	template <typename M, typename V>  
	inline void MapToVec( const  M & m, V & v ) { 
		for( typename M::const_iterator it = m.begin(); it != m.end(); ++it ) { 
			v.push_back( it->second ); 
		} 
	} 

}

inline std::string file2string(std::string filename )
{
	std::ifstream t( filename );
	std::string str;

	t.seekg( 0, std::ios::end );
	str = std::string( t.tellg(), ' ' );
	t.seekg( 0, std::ios::beg );

	str.assign( ( std::istreambuf_iterator<char>( t ) ), std::istreambuf_iterator<char>() );
	return str;
}

inline std::string itoa_cpp(int value, int base)
{                                                                                                                                                        
	std::string buf;                                                                                                                                     

	// check that the base if valid                                                                                                                      
	if (base < 2 || base > 16)                                                                                                                           
		return buf;                                                                                                                                      

	enum { kMaxDigits = 35 };                                                                                                                            
	buf.reserve( kMaxDigits );  // Pre-allocate enough space.                                                                                            

	int quotient = value;                                                                                                                                

	// Translating number to string with base:                                                                                                           
	do {                                                                                                                                                 
		buf += "0123456789abcdef"[ std::abs( quotient % base ) ];                                                                                        
		quotient /= base;                                                                                                                                
	} while ( quotient );                                                                                                                                

	// Append the negative sign                                                                                                                          
	if ( value < 0 )                                                                                                                                     
		buf += '-';                                                                                                                                      

	std::reverse( buf.begin(), buf.end() );                                                                                                              
	return buf;                                                                                                                                          
}