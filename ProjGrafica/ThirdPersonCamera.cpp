#include "stdafx.h"
#include "ThirdPersonCamera.h"

using namespace flee;
ThirdPersonCamera::ThirdPersonCamera(void):Camera()
{
	following = NULL;
}


ThirdPersonCamera::~ThirdPersonCamera(void)
{
}

void ThirdPersonCamera::Follow(Model * m)
{
	if(this->following == NULL)
		this->following = m;
	//m->
}

void ThirdPersonCamera::Translate(float3 t)
{
	//Camera::Translate(t);
	float msec = (Camera::gameTimer)->Get_dt()/1000.f;
	if(following != NULL)
		this->following->Translate(-t.toVec3(),msec, rotationX, rotationY, rotationZ);
	float3 a(this->following->GetPosition().x,this->following->GetPosition().y,this->following->GetPosition().z-4);
	Camera::SetPosition(a);
	//viewMatrix = glm::lookAt(position.toVec3(),this->following->GetPosition(), _up);
	//RecalculateMatrix();
}

void ThirdPersonCamera::LookAt(int x, int y)
{
	Camera::CalcRotation(x,y);
	/*if(following != NULL)
		this->following->Rotate(this->rotationX,this->rotationY,this->rotationZ);*/
	float xp = 5.f * sin((rotationX)) + this->following->GetPosition().x;
	float yp = 5.f * cos((rotationX)) + this->following->GetPosition().z;
	viewMatrix = glm::lookAt(glm::vec3(xp,1,yp), this->following->GetPosition(),glm::vec3(0,1,0));
	
	RecalculateMatrix();
}

void ThirdPersonCamera::SetPosition(float3 t)
{
	if(following != NULL)
		this->following->SetPosition(t.toVec3());
	Camera::SetPosition(t);
}

float3 ThirdPersonCamera::FakeTranslate(float3 t)
{
float3 futurepos(position.x,position.y,position.z);
	
	float msec = (gameTimer)->Get_dt()/1000.0f;
	if(t.z!= 0.0f && t.x != 0.0f) {
		futurepos.x += this->CameraSpeed * sin(rotationX) * msec * t.z * t.x;
		futurepos.z += this->CameraSpeed * cos(rotationX) * msec * t.z * t.x;	
	} else {
		if( t.x != 0.0f)
		{
			futurepos.x += this->CameraSpeed * cos(rotationX) * msec * t.x;
			futurepos.z -= this->CameraSpeed * sin(rotationX) * msec * t.x;	
		}
		if(t.z != 0.0f)
		{
			futurepos.x += this->CameraSpeed * sin(rotationX) * msec * t.z;
			futurepos.z += this->CameraSpeed * cos(rotationX) * msec * t.z;
		}
	}
	if(t.y != -0.f){
		futurepos.y += this->CameraSpeed * msec * t.y;
	}
	return futurepos;
}