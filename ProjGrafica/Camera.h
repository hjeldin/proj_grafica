#pragma once
#include "defs.h"
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtx/quaternion.hpp>
#include "ShaderManager.h"
#include "CTime.h"
#include "Model.h"
namespace flee{
	class Camera
	{
	public:
		Camera(void);
		~Camera(void);
		void SetPosition(float3 pos);
		float3 GetPosition();
		void Translate(float3 t);
		float3 FakeTranslate(float3 t);
		void LookAt(int x, int y);
		void SetTimer(void * timer);
		float CameraSpeed;
		const glm::mat4 GetViewMatrix();
		void CalcRotation(int x, int y);
		float3 GetTarget();
		float3 GetUp();
		void Update();
		void Follow(Model *);
		CameraType type;
		Model * following;
		void SetType(CameraType c);
	private:
	protected:
		float rotationX,rotationY,rotationZ;
		float3 Unproject(int, int);
		void RecalculateMatrix();
		float3 position;
		float3 rotation;
		glm::vec3 _direction,_up,_right;
		glm::vec3 target;
		glm::mat4 viewMatrix;
		mytimer* gameTimer;
	};

}