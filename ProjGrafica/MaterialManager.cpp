#include "stdafx.h"
#include "defs.h"
#include "MaterialManager.h"
#include "IL\il.h"
#include "IL\ilu.h"
using namespace flee;

MaterialManager * MaterialManager::instance_ptr = NULL;

MaterialManager::MaterialManager(void)
{
	ilInit();
	iluInit();
}


MaterialManager::~MaterialManager(void)
{
	std::map<std::string, Material *>::iterator it;
	for(it = materialMap.begin();it!=materialMap.end();it++)
	{
		(*(it->second)).Shutdown();
	}
	Purge();
	ilShutDown();
}

void MaterialManager::Add(Material * mat)
{
	std::map<std::string, Material *>::iterator it;
	it = materialMap.end();
	int matUid = reinterpret_cast<int>(mat);
	materialMap.insert(it,std::map<std::string, Material *>::value_type(std::string(itoa_cpp(matUid,10)),mat));
}

void MaterialManager::Remove(Material * mat)
{
	std::map<std::string, Material *>::iterator it;
	it = materialMap.begin();
	int matUid = reinterpret_cast<int>(mat);
	materialMap.erase(itoa_cpp(matUid,10));
}

void MaterialManager::Purge()
{
	materialMap.erase(materialMap.begin(), materialMap.end());
}

Material * MaterialManager::Get(std::string &str)
{
	return materialMap.find(str)->second;
}