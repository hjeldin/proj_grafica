#pragma once

#include "defs.h"
#include <vector>
#include <string>
#include <fstream>
#include "assimp.hpp"      // C++ importer interface
#include "aiScene.h"       // Output data structure
#include "aiPostProcess.h" // Post processing flags
#include "ShaderManager.h"
#include "IL\il.h"
#include <direct.h> 

namespace flee{
	enum ShaderType
	{
		FRAGMENT,
		VERTEX,
		GEOMETRY
	};

	class Material
	{
	public:
		Material(void);
		Material(aiMaterial * material);
		~Material(void);

		void Initialize(void);

		void SetAmbientColor(float4 ac){ this->ambientColor = ac; }
		void SetDiffuseColor(float4 dc){ this->diffuseColor = dc; }
		void SetSpecularColor(float4 sc){ this->specularColor = sc; }
		void SetEmissiveColor(float4 ec){ this->emissiveColor = ec; }
		float4 GetAmbientColor(void){ return this->ambientColor; }
		float4 GetDiffuseColor(void){ return this->diffuseColor; }
		float4 GetSpecularColor(void){ return this->specularColor; }
		float4 GetEmissiveColor(void){ return this->emissiveColor; }

		void SetSmooth(){ this->smooth = true; }
		void SetFlat(){ this->smooth = false; }

		void SetShader(std::string shaderFile, ShaderType t){ this->shaders[t] = file2string(shaderFile); }
		void SetProgram(GLuint shaderProgram){ this->shaderProgram = shaderProgram; }
		GLuint GetShaderProgram(void){ return this->shaderProgram; }
		void UseProgram(void);
		void StopProgram(void);
		bool IsEnabled(void) { return enabled; }
		void Enable(void) { enabled = true; }
		void Disable(void) { enabled = false; }
		void Shutdown(void);
		void SetVAO(GLuint vao){mVAO = vao;}
		GLuint GetVAO(void){return mVAO;}
		bool LoadTexture(std::string filename);
		bool LoadNormalMap(std::string filename);
		void LoadMaterial(std::string, std::string);
		void CheckError(void)
		{
#if DEBUG
			GLenum a = glGetError();
			if(a != GL_NO_ERROR){
				std::cout << a <<std::endl;
			}
#endif
		}
	private:
		aiMaterial * mat;
		std::string file2string(std::string filename );
		float ambientIntensity;
		float4 ambientColor, diffuseColor, specularColor, emissiveColor;
		bool smooth;
		GLuint shaderProgram;
		GLuint mFs, mVs;
		std::string shaders[3];
		bool enabled;
		GLuint mVAO;
		GLuint texture,normalTex;
		GLuint diffuseLoc, normalLoc, shadowLoc;
		ILuint texName;
	};

}