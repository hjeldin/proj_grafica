#pragma once
#include "defs.h"
#include <vector>
#include "BufferManager.h"
#include "assimp.hpp"      // C++ importer interface
#include "aiScene.h"       // Output data structure
#include "aiPostProcess.h" // Post processing flags
namespace flee{
	class BufferManager;
	class Mesh
	{
	public:
		Mesh(void);
		Mesh(aiMesh * pMesh);
		Mesh(std::vector<vertex> vertices, std::vector<int> indices);
		~Mesh(void);
		void Initialize(void);
		void Shutdown(void);
		void Draw(void);
		bool IsLoaded(void){ return this->isLoaded; }
		unsigned int GetIOffset(){ return this->indexOffset; }
		void SetIOffset(unsigned int off);
		unsigned int GetVOffset(){ return this->vertexOffset; }
		void SetVOffset(unsigned int off);
		std::vector<vertex> * GetVertices(void){ return &vertices; }
		std::vector<int> * GetIndices(void){ return &indices; }
		unsigned long int size;

		void SetVAO(GLuint v) { this->mVAO = v; }
		GLuint GetVAO(){ return this->mVAO; }
		GLuint GetVBO(){ return this->mVBO; }
		void CheckErrors(void ){
#if DEBUG
			GLenum a = glGetError();
			if(a != GL_NO_ERROR){
				std::cout << glewGetErrorString(a) << "from mesh.h"<< std::endl;
			}
#endif
		}
		void InitializeBuffers(void);
		void InitializeBuffersVertex(void);
	private:
		GLuint mVAO,mVBO;
		BufferManager * bManager;
		
		aiMesh * mMesh;

		bool isLoaded;
		
		void ShutdownBuffers(void);
		unsigned int indexOffset;
		unsigned int vertexOffset;
		std::vector<vertex> vertices;
		std::vector<int> indices;

		unsigned long int m_vertexCount;
		unsigned int m_indexCount;
	};
}

