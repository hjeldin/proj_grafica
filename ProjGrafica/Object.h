#pragma once
namespace flee{
	class Object {
	public:
		float x;
		float y;
		float width;
		float height;
		Object( float x, float y, float width, float height );
	};
}