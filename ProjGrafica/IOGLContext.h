#pragma once
namespace flee{
	class IOGLContext
	{
	public:
		virtual bool create30Context(void) = 0;	// Creation of our OpenGL 3.x context
		virtual bool createContext(void) = 0;		// Creation of standard OpenGL context
		virtual bool InitializeExtensions(void) = 0;		// Initialize opengl extension wrangler
		virtual void setupScene(void) = 0;				// All scene information can be setup here
		virtual void reshapeWindow(int w, int h) = 0;	// Method to get our window width and height on resize
	};
}