#pragma once
#include "Model.h"

namespace flee{
	/**
		StaticShadowMap creates a shadowmap in an FBO.
		Documentation can be found at http://www.paulsprojects.net/tutorials/smt/smt.html
		\warning{name will be changed as soon as VS will support fucking refactoring}
	*/
	class StaticShadowMap
	{
	public:
		/**
			Default constructor
		*/
		StaticShadowMap(void);
		/**
			Default deconstructor
		*/
		~StaticShadowMap(void);
		/**
			Initializes the shaders and generates the FBO
		*/
		void Init(void);
		/**
			Creates an fbo and attach a texture
		*/
		void GenerateFBO(void);
		/**
			Should generate a cubemap texture for pointlight 
		*/
		void GenerateCubeMap(void);
		/**
			Creates the shadowmap shader
		*/
		void AttachShader(void);
		/**
			Renders specified mesh to shadow texture
		*/
		void Render();
		/**
			Deinitializes the shadow buffer. 
			Clears textures, shaders, framebuffer
		*/
		void Deinit(void);
		/**
			Getter for shadow map texture
		*/
		GLuint GetShadowTexture(void);
		/**
			Set texture dimensions
			\param width
			\param height
		*/
		void SetDimensions(int,int);
		/**
			Setter for light position.
			\param x
			\param y
			\param z
		*/
		void SetLightPosition(int, int, int);
		/**
			Setter for light direction
			\param x
			\param y
			\param z
		*/
		void SetLightDirection(int, int, int);
		/**
			Setter for the mesh that will be rendered
			\param mesh pointer
		*/
		void SetStaticMesh(Mesh *);

		/**
			Getter for shadow modelviewprojection
			\return the MVP biased for texture lookup
		*/
		glm::mat4 GetShadowMVP();
	private:
		glm::mat4 projection, view,model;
		Mesh * toDraw;
		GLuint texXP,texXN,texYP,texYN,texZP,texZN;
		GLuint texID,fboID,pShader,vShader,program,colorTexID,depthID;
		int width,height;
		float3 lightpos;
		float3 lightdir;
	};
};
