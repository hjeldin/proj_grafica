#include "stdafx.h"
#include "SkyBox.h"

using namespace flee;
SkyBox::SkyBox(void)
{
}


SkyBox::~SkyBox(void)
{
	glDeleteTextures(1,&mTextureObj);
	glUseProgram(this->shaderProgram);
	glDeleteShader(this->mVs);
	glDeleteShader(this->mFs);
	//glDeleteProgram(this->shaderProgram);
}

bool SkyBox::Load(std::string xp,std::string xn, std::string yp, std::string yn, std::string zp, std::string zn)
{
	//glEnable(GL_TEXTURE_CUBE_MAP);
	glGenTextures(1, &mTextureObj);
	glBindTexture(GL_TEXTURE_CUBE_MAP, mTextureObj);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	//glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP );

	int successes=0;
	bool success;

	char cCurrentPath[FILENAME_MAX];
	if (! _getcwd(cCurrentPath, sizeof(cCurrentPath) / sizeof(TCHAR))) 
	{ 
		return errno; 
	}
	cCurrentPath[sizeof(cCurrentPath) - 1] = '\0'; /* not really required */ 
	ilGenImages(1, &mILObj); 
	ilBindImage(mILObj);

	// LOAD XPOS
	std::string fullPath = std::string(cCurrentPath).append(std::string("\\").append(xp));
	success = ilLoadImage((wchar_t*)fullPath.c_str()); 
	if (success) /* If no error occured: */
	{
		successes++;
		success = ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE); /* Convert every colour component into
															unsigned byte. If your image contains alpha channel you can replace IL_RGB with IL_RGBA */
		if (!success)
		{
			/* Error occured */
			MessageBox(NULL,
				(LPCWSTR)L"Could not load texture",
				(LPCWSTR)L"Bye!",
				MB_ICONERROR| MB_OK | MB_DEFBUTTON2);
			exit(1);
			return false;
		}
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGB, 512, 512, 0, GL_RGB, GL_UNSIGNED_BYTE, ilGetData());    
		successes++;
	}
	ilDeleteImages(1, &mILObj);

	//LOAD XN
	fullPath = std::string(cCurrentPath).append(std::string("\\").append(xn));
	success = ilLoadImage((wchar_t*)fullPath.c_str()); 
	if (success) /* If no error occured: */
	{
		successes++;
		success = ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE); /* Convert every colour component into
															unsigned byte. If your image contains alpha channel you can replace IL_RGB with IL_RGBA */
		if (!success)
		{
			/* Error occured */
			MessageBox(NULL,
				(LPCWSTR)L"Could not load texture",
				(LPCWSTR)L"Bye!",
				MB_ICONERROR| MB_OK | MB_DEFBUTTON2);
			exit(1);
			return false;
		}
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGB,512, 512, 0, GL_RGB, GL_UNSIGNED_BYTE, ilGetData());    
		successes++;
	}
	ilDeleteImages(1, &mILObj); 

	//LOAD YPOS
	fullPath = std::string(cCurrentPath).append(std::string("\\").append(yp));
	success = ilLoadImage((wchar_t*)fullPath.c_str()); 
	if (success) /* If no error occured: */
	{
		successes++;
		success = ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE); /* Convert every colour component into
															unsigned byte. If your image contains alpha channel you can replace IL_RGB with IL_RGBA */
		if (!success)
		{
			/* Error occured */
			MessageBox(NULL,
				(LPCWSTR)L"Could not load texture",
				(LPCWSTR)L"Bye!",
				MB_ICONERROR| MB_OK | MB_DEFBUTTON2);
			exit(1);
			return false;
		}
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGB,512, 512, 0, GL_RGB, GL_UNSIGNED_BYTE, ilGetData()); 
		successes++;
	}
	ilDeleteImages(1, &mILObj); 

	//LOAD YNEG
	fullPath = std::string(cCurrentPath).append(std::string("\\").append(yn));
	success = ilLoadImage((wchar_t*)fullPath.c_str()); 
	if (success) /* If no error occured: */
	{
		successes++;
		success = ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE); /* Convert every colour component into
															unsigned byte. If your image contains alpha channel you can replace IL_RGB with IL_RGBA */
		if (!success)
		{
			/* Error occured */
			MessageBox(NULL,
				(LPCWSTR)L"Could not load texture",
				(LPCWSTR)L"Bye!",
				MB_ICONERROR| MB_OK | MB_DEFBUTTON2);
			exit(1);
			return false;
		}
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGB,512, 512, 0, GL_RGB, GL_UNSIGNED_BYTE, ilGetData());    
		successes++;
	}
	ilDeleteImages(1, &mILObj); 

	//LOAD ZPOS
	fullPath = std::string(cCurrentPath).append(std::string("\\").append(zp));
	success = ilLoadImage((wchar_t*)fullPath.c_str()); 
	if (success) /* If no error occured: */
	{
		successes++;
		success = ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE); /* Convert every colour component into
															unsigned byte. If your image contains alpha channel you can replace IL_RGB with IL_RGBA */
		if (!success)
		{
			/* Error occured */
			MessageBox(NULL,
				(LPCWSTR)L"Could not load texture",
				(LPCWSTR)L"Bye!",
				MB_ICONERROR| MB_OK | MB_DEFBUTTON2);
			exit(1);
			return false;
		}
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGB,512, 512, 0, GL_RGB, GL_UNSIGNED_BYTE, ilGetData()); 
		successes++;
	}
	ilDeleteImages(1, &mILObj); 

	//LOAD ZNEG
	fullPath = std::string(cCurrentPath).append(std::string("\\").append(zn));
	success = ilLoadImage((wchar_t*)fullPath.c_str()); 
	if (success) /* If no error occured: */
	{
		successes++;
		success = ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE); /* Convert every colour component into
															unsigned byte. If your image contains alpha channel you can replace IL_RGB with IL_RGBA */
		if (!success)
		{
			/* Error occured */
			MessageBox(NULL,
				(LPCWSTR)L"Could not load texture",
				(LPCWSTR)L"Bye!",
				MB_ICONERROR| MB_OK | MB_DEFBUTTON2);
			exit(1);
			return false;
		}
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGB,512, 512, 0, GL_RGB, GL_UNSIGNED_BYTE, ilGetData());    
		successes++;
	}
	ilDeleteImages(1, &mILObj); 



	//LOAD SHADERS

	this->shaders[0] = file2string(std::string("shaders\\skybox.ps"));
	this->shaders[1] = file2string(std::string("shaders\\skybox.vs"));
	mFs = ShaderManager::get_instance()->CompileShaderOfType(GL_FRAGMENT_SHADER, this->shaders[0]);
	mVs = ShaderManager::get_instance()->CompileShaderOfType(GL_VERTEX_SHADER, this->shaders[1]);
	this->shaderProgram = ShaderManager::get_instance()->LinkShaders(mVs,mFs,0);
	this->shaderProgram = ShaderManager::get_instance()->GetProgram(this->shaderProgram);
	glUseProgram(this->shaderProgram);
	modelMatrix = glGetUniformLocation(shaderProgram, "u_model");
	viewMatrix = glGetUniformLocation(shaderProgram, "u_view");
	projectionMatrix = glGetUniformLocation(shaderProgram, "u_projection");
	GLuint m_textureLocation = glGetUniformLocation(shaderProgram, "gCubemapTexture");
	glUniform1i(m_textureLocation, 0);


	//CREATE MESH
	/*std::vector<vertex> vertices;
	std::vector<int> indices;

	vertices.push_back(vertex(float4(-1.0f,1.f,1.f,1.f),float3(0.f,0.f,0.f),-1.f,1.f,0.f));
	vertices.push_back(vertex(float4(-1.0f,-1.f,1.f,1.f)));
	vertices.push_back(vertex(float4(1.0f,-1.f,1.f,1.f)));
	vertices.push_back(vertex(float4(1.0f,1.f,1.f,1.f)));
	vertices.push_back(vertex(float4(-1.0f,1.f,-1.f,1.f)));
	vertices.push_back(vertex(float4(-1.0f,-1.f,1.f,1.f)));
	vertices.push_back(vertex(float4(1.0f,-1.f,-1.f,1.f)));
	vertices.push_back(vertex(float4(1.0f,1.f,-1.f,1.f)));

	indices.push_back(0);
	indices.push_back(1);
	indices.push_back(2);
	indices.push_back(3);
	indices.push_back(3);
	indices.push_back(2);
	indices.push_back(6);
	indices.push_back(7);
	indices.push_back(4);
	indices.push_back(5);
	indices.push_back(1);
	indices.push_back(0);
	indices.push_back(0);
	indices.push_back(3);
	indices.push_back(7);
	indices.push_back(4);
	indices.push_back(1);
	indices.push_back(2);
	indices.push_back(5);
	indices.push_back(6);
	this->m_pMesh = new Mesh(vertices,indices);
	this->m_pMesh->Initialize();
	if(successes < 10){
	MessageBox(NULL,
	(LPCWSTR)L"LOL!",
	(LPCWSTR)L"Bye!",
	MB_ICONERROR| MB_OK | MB_DEFBUTTON2);
	exit(1);
	return false;
	}*/
	return true; // XXX: should effectively return something useful
}

void SkyBox::Bind(GLenum TextureUnit)
{
	glActiveTexture(TextureUnit);
	glBindTexture(GL_TEXTURE_CUBE_MAP, mTextureObj);
}

void SkyBox::Render()
{
	GLint OldCullFaceMode;
	glGetIntegerv(GL_CULL_FACE_MODE, &OldCullFaceMode);
	GLint OldDepthFuncMode;
	glGetIntegerv(GL_DEPTH_FUNC, &OldDepthFuncMode);

	glCullFace(GL_FRONT);
	glDepthFunc(GL_LEQUAL);
	this->Bind(GL_TEXTURE0);

	this->m_pMesh->Draw();

	glCullFace(OldCullFaceMode); 
	glDepthFunc(OldDepthFuncMode);
}

void SkyBox::SetProjection(glm::mat4x4 proj)
{
	glUniformMatrix4fv(projectionMatrix,1,GL_FALSE,glm::value_ptr(proj));
}
void SkyBox::SetView(glm::mat4x4 view)
{
	glUniformMatrix4fv(viewMatrix,1,GL_FALSE,glm::value_ptr(view));
}
void SkyBox::SetModel(glm::vec3 model)
{
	glm::mat4 modelM = glm::translate(glm::mat4(),model);
	glm::mat4 scaleM = glm::scale(glm::mat4(),glm::vec3(10.f,10.f,10.f));
	glm::mat4 res = modelM  * scaleM ;
	glUniformMatrix4fv(modelMatrix,1,GL_FALSE,glm::value_ptr(res));
}
std::string SkyBox::file2string(std::string filename )
{
	std::ifstream t( filename );
	std::string str;

	t.seekg( 0, std::ios::end );
	str = std::string( t.tellg(), ' ' );
	t.seekg( 0, std::ios::beg );

	str.assign( ( std::istreambuf_iterator<char>( t ) ), std::istreambuf_iterator<char>() );
	return str;
}

void SkyBox::SetActive()
{
	glUseProgram(this->shaderProgram);
}