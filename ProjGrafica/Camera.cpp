#include "stdafx.h"
#include "Camera.h"

using namespace flee;

Camera::Camera():CameraSpeed(5.0f),type(FIRST_PERSON)
{
	this->position = float3(3,0,5);
	glm::vec3 tmpPos(position.x, position.y, position.z);
	viewMatrix = glm::lookAt(tmpPos,glm::vec3(5,5,0),glm::vec3(0,-1,0));
	//rotationX = rotationY = rotationZ = 0;
	rotationX = 0.9f;
	rotationY = 0.1f;
	following = NULL;
}


Camera::~Camera(void)
{
}
void Camera::Update()
{
	/*if((gameTimer)->GetTime() > 5000)
	this->position.y -= (((gameTimer)->GetTime())-5000.f)/1000.f;*/
}

void Camera::RecalculateMatrix()
{
	//glm::vec3 tmpPos(position.x, position.y, position.z);
	//std::cout << rotationX/180*3.14 << std::endl;
	//viewMatrix = glm::lookAt(tmpPos, tmpPos+_direction, _up);
	ShaderManager::get_instance()->SetUniforms(viewMatrix);
	if(type == FIRST_PERSON)
		ShaderManager::get_instance()->SetPlayerPos(position.toVec3());
	else 
		ShaderManager::get_instance()->SetPlayerPos(this->following->GetPosition());
}

void Camera::SetPosition(float3 pos)
{
	this->position = pos;
	RecalculateMatrix();
}

float3 Camera::GetPosition()
{
	return this->position;
}

void Camera::Translate(float3 t)
{
	float msec = (gameTimer)->Get_dt()/1000.0f;
	if(type == FIRST_PERSON){
		if(t.z!= 0.0f && t.x != 0.0f) {
			position.x += CameraSpeed * sin(rotationX) * msec * t.z * t.x;
			position.z += CameraSpeed * cos(rotationX) * msec * t.z * t.x;	
		} else {
			if( t.x != 0.0f)
			{
				position.x += CameraSpeed * cos(rotationX) * msec * t.x;
				position.z -= CameraSpeed * sin(rotationX) * msec * t.x;	
			}
			if(t.z != 0.0f)
			{
				position.x += CameraSpeed * sin(rotationX) * msec * t.z;
				position.z += CameraSpeed * cos(rotationX) * msec * t.z;
			}
		}
		if(t.y != -0.f){
			position.y += CameraSpeed * msec * t.y;
		}
	} else {
		if(t.y != 0.0f) t.y = -t.y;
		if(following != NULL)
			this->following->Translate(-t.toVec3(),msec, rotationX, rotationY, rotationZ);
		float3 a(this->following->GetPosition().x,this->following->GetPosition().y,this->following->GetPosition().z-1);
		SetPosition(a);
	}
	
	RecalculateMatrix();
}

float3 Camera::FakeTranslate(float3 t)
{
	float3 futurepos;
	if(type==FIRST_PERSON)

		futurepos = float3(position.x,position.y,position.z);
	else 
		futurepos = float3(following->GetPosition().x,following->GetPosition().y,following->GetPosition().z);
	/*glm::vec3 t2(t.x,t.y,t.z);
	t2 *= _direction;
	std::cout << _direction.x << _direction.y<< _direction.z << std::endl;
	this->position = position + float3(t2.x,t2.y,t2.z);
	viewMatrix = glm::translate(viewMatrix, glm::vec3(t2.x,t2.y,t2.z));
	*/
	//position.x += t.x * cos(rotationY) -(t.z * sin(rotationY));
	//position.y += t.y * cos(rotationX);
	//position.z += t.x * sin(rotationY) +(t.z * cos(rotationY));
	//glm::vec3 t2(t.x,t.y,t.z);
	//viewMatrix = glm::translate(viewMatrix, t.x * cos(rotationY) -(t.z * sin(rotationY)),(t.y * cos(rotationX)),(t.x * sin(rotationY) +(t.z * cos(rotationY))));
	//std::cout << sin(rotationX)<< std::endl;
	float msec = (gameTimer)->Get_dt()/1000.0f;
	//std::cout << msec << std::endl;
	//msec = 0.5f;
	if(t.z!= 0.0f && t.x != 0.0f) {
		futurepos.x += CameraSpeed * sin(rotationX) * msec * t.z * t.x;
		futurepos.z += CameraSpeed * cos(rotationX) * msec * t.z * t.x;	
	} else {
		if( t.x != 0.0f)
		{
			futurepos.x += CameraSpeed * cos(rotationX) * msec * t.x;
			futurepos.z -= CameraSpeed * sin(rotationX) * msec * t.x;	
		}
		if(t.z != 0.0f)
		{
			futurepos.x += CameraSpeed * sin(rotationX) * msec * t.z;
			futurepos.z += CameraSpeed * cos(rotationX) * msec * t.z;
		}
	}
	if(t.y != -0.f){
		futurepos.y += CameraSpeed * msec * t.y;
	}
	return futurepos;
	//position.y 
	//position.y += _direction.y;
	//position.z += t.z * _direction.z;
	/*target.x = position.x;
	target.y = position.y;
	target.z = position.z;
	target += _direction;
	glm::vec3 t2(position.x,position.y,position.z);*/
	//viewMatrix = glm::lookAt(t2, target, _up);
	//RecalculateMatrix();
}

void Camera::CalcRotation(int x, int y)
{
	rotationX -= x * 0.01f;
	rotationY -= y * 0.01f;
	if(rotationY > 3.14f/2.0f) rotationY = 3.14f/2.0f;
	if(rotationX > 3.14f*2.0f) rotationX = 0;
	if(rotationY < -3.14f/2.0f) rotationY = -3.14f/2.0f;
	if(rotationX < 0) rotationX = 3.14f*2;
}

void Camera::LookAt(int x, int y){
	CalcRotation(x,y);
	if(type == FIRST_PERSON){
		//std::cout << x << y<< rotationX << rotationY <<std::endl;
		_direction = glm::vec3( cos(rotationY) * sin(rotationX), 
			sin(rotationY), 
			cos(rotationY) * cos(rotationX) ); 

		_right = glm::vec3( sin(rotationX - 3.14f/2.0f), 
			0, 
			cos(rotationX - 3.14f/2.0f) ); 

		_up = glm::cross( _right, _direction ); 


		target.x = position.x;
		target.y = position.y;
		target.z = position.z;
		target += _direction;
		glm::vec3 tmpPos(position.x, position.y, position.z);
		viewMatrix = glm::lookAt( tmpPos, target, _up ); 

	} else {
		float xp = 2.5f * sin((rotationX)) + this->following->GetPosition().x;
		float yp = 2.5f * cos((rotationX)) + this->following->GetPosition().z;
		viewMatrix = glm::lookAt(glm::vec3(xp,1,yp), this->following->GetPosition(),glm::vec3(0,1,0));
		this->following->SetRotation(glm::vec3(0.f,(rotationX*180.f/3.14f)-180.f,0.f));
	}
	RecalculateMatrix();
}

float3 Camera::Unproject(int x, int y)
{
	//glm::mat4 unprojMat = glm::inverse(glm::perspective(60.0f,4.0f/3.0f,0.1f,1000.0f))
	return float3(0,0,0);
}

void Camera::SetTimer(void * timer)
{
	this->gameTimer = (mytimer *)timer;
}

const glm::mat4 Camera::GetViewMatrix()
{
	return this->viewMatrix;
}

float3 Camera::GetTarget()
{
	return float3(this->target.x,this->target.y,this->target.z);
}

float3 Camera::GetUp()
{
	return float3(this->_up.x, this->_up.y, this->_up.z);
}

void Camera::Follow(Model * m)
{
	if(this->following == NULL)
		this->following = m;
}

void Camera::SetType(CameraType c)
{
	if(c == FIRST_PERSON)
		following->visible = false;
	else 
		following->visible = true;

	type = c;
}